package main

import (
	"log"
	"net/http"

	"bitbucket.org/lriveros/open-iptv/models"
	"github.com/gorilla/securecookie"
)

// cookie handling
var cookieHandler = securecookie.New(
	securecookie.GenerateRandomKey(64),
	securecookie.GenerateRandomKey(32))

//
type Message struct {
	ShowMessage bool
	Title       string
	Detail      string
	TypeMsge    string
}

//Context
type Context struct {
	User     models.User
	MainURL  string
	Message  Message
	Channel  models.Channel
	Channels []models.Channel
}

func initContext(url string) Context {
	var err error
	var ctx Context
	ctx.MainURL = url + urlMainList
	ctx.Channels, err = models.GetChannelList("")
	if err != nil {
		log.Print(err)
	}
	return ctx
}

func getSessionContext(request *http.Request) Context {
	var ctx Context
	if cookie, err := request.Cookie("session"); err == nil {
		if err = cookieHandler.Decode("session", cookie.Value, &ctx); err != nil {
			log.Print("Error con las cookies (Iniciando nuevo contexto): ", err)
			ctx = initContext(request.Host)
		}
	}
	if ctx.MainURL == "" {
		ctx = initContext(request.Host)
	}
	ctx.Message.ShowMessage = false
	return ctx
}

//struct User := models.User;
func getSessionUser(request *http.Request) models.User {
	ctx := getSessionContext(request)
	return ctx.User
}

func setSession(ctx Context, response http.ResponseWriter) {
	if encoded, err := cookieHandler.Encode("session", ctx); err == nil {
		cookie := &http.Cookie{
			Name:  "session",
			Value: encoded,
			Path:  "/",
		}
		http.SetCookie(response, cookie)
	}
}

func clearSession(response http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:   "session",
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}
	http.SetCookie(response, cookie)
}
