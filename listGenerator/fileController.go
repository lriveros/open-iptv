package listGenerator

import (
	"io"
	"log"
	"os"
)

func createFile(path string) bool {
	// detect if file exists
	var _, err = os.Stat(path)

	// create file if not exists
	if os.IsNotExist(err) {
		var file, err = os.Create(path)
		if isError(err) {
			return false
		}
		defer file.Close()
	}

	log.Println("==> Done creating file", path)
	return true
}

func writeFile(path string, txt string) bool {
	// open file using READ & WRITE permission
	var file, err = os.OpenFile(path, os.O_RDWR, 0644)
	if isError(err) {
		return false
	}
	defer file.Close()

	// write some text line-by-line to file
	_, err = file.WriteString(txt)
	if isError(err) {
		return false
	}

	// save changes
	err = file.Sync()
	if isError(err) {
		return false
	}

	log.Println("==> Done writing to file", path)
	return true
}

func readFile(path string) string {
	// re-open file
	var file, err = os.OpenFile(path, os.O_RDWR, 0644)
	if isError(err) {
		return ""
	}
	defer file.Close()

	// read file, line by line
	var text = make([]byte, 1024)
	for {
		_, err = file.Read(text)

		// break if finally arrived at end of file
		if err == io.EOF {
			break
		}

		// break if error occured
		if err != nil && err != io.EOF {
			isError(err)
			break
		}
	}

	log.Println("==> Done reading from file", path)

	return string(text)
}

func deleteFile(path string) bool {
	// delete file
	var err = os.Remove(path)
	if isError(err) {
		return false
	}

	log.Println("==> Done deleting file", path)
	return true
}

func renameFile(path string, newPath string) bool {
	// rename file
	var err = os.Rename(path, newPath)
	if isError(err) {
		return false
	}

	log.Println("==> done renaming file", path, ">", newPath)
	return true
}

func isError(err error) bool {
	if err != nil {
		log.Println(err.Error())
	}

	return (err != nil)
}
