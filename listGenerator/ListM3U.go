package listGenerator

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"reflect"

	"bitbucket.org/lriveros/open-iptv/models"
)

const (
	headerList = "#EXTM3U\n"
	bodyList   = "#EXTINF:-1 tvg-logo=\"%s\" group-title=\"%s\" , %s \n%s\n"
	footerList = ""
)

func GenerateList(fileName string, chnlList []models.ChannelList) {
	log.Println("Iniciando secuencia de generacion de lista M3U")

	appUrl := os.Getenv("APP_URL")
	if appUrl == "" {
		//log.Fatal("$PORT must be set")
		appUrl = "https://open-iptv.herokuapp.com/"
	}
	appUrl = appUrl + "imgs/channels_logo/"
	var buffer bytes.Buffer
	buffer.WriteString(headerList)
	for _, c := range chnlList {
		str := fmt.Sprintf(bodyList, appUrl+c.Logo, c.Groups, c.Name, c.Url)
		buffer.WriteString(str)
	}
	buffer.WriteString(footerList)
	//fmt.Print(buffer.String())
	fileTemp := fileName + ".tmp"
	fileBkp := fileName + ".bkp"

	log.Println("Cadenas de escritura lista, crenado archivos")
	createFile(fileTemp)
	writeFile(fileTemp, buffer.String())
	renameFile(fileName, fileBkp)
	renameFile(fileTemp, fileName)
	deleteFile(fileBkp)
	log.Printf("Secuencia de generacion de lista terminada")
}

func CheckListUrls(linkList []models.Link) {
	log.Println(":::: Secuencia de validacion de links > Iniciada")
	var una bool
	for _, lnk := range linkList {
		log.Println(":: Validando Link ID > ", lnk.ID)
		valid := verifyLink(lnk.Url)
		if !valid {
			una = true
			log.Println(":: Link de canal inactivo > ", lnk.ID)
			models.UpdateLink(lnk.ID, 0)
		}
	}
	if !una {
		log.Println("> No se encontraron canales por desactivar")
	}
	log.Printf(":::: Secuencia de validacion de links : Terminada")
}

func verifyLink(url string) bool {
	bad_codes := []int{404, 500, 403}
	response, err := http.Get(url)
	if err != nil {
		return false
	}
	defer response.Body.Close()
	_, err = ioutil.ReadAll(response.Body)
	//TODO: Validar links internos
	//fmt.Println("Response Status Code ### ", response.StatusCode)
	//fmt.Println("Response ### ", string(body))
	erroneo, _ := in_array(response.StatusCode, bad_codes)
	if err != nil || erroneo {
		return false
	}
	return true
}

func in_array(val interface{}, array interface{}) (exists bool, index int) {
	exists = false
	index = -1

	switch reflect.TypeOf(array).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(array)

		for i := 0; i < s.Len(); i++ {
			if reflect.DeepEqual(val, s.Index(i).Interface()) == true {
				index = i
				exists = true
				return
			}
		}
	}

	return
}
