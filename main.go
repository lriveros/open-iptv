package main

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/lriveros/open-iptv/listGenerator"
	"bitbucket.org/lriveros/open-iptv/models"
	"github.com/gorilla/mux"
	"github.com/robfig/cron"
)

var counts SafeCounter

func main() {
	port := os.Getenv("PORT")

	if port == "" {
		//log.Fatal("$PORT must be set")
		port = "80"
	}

	counts = SafeCounter{v: make(map[string]int)}

	cssHandler := http.FileServer(http.Dir("./static/css/"))
	fontsHandler := http.FileServer(http.Dir("./static/fonts/"))
	jsHandler := http.FileServer(http.Dir("./static/js/"))
	imgsHandler := http.FileServer(http.Dir("./static/imgs/"))

	http.Handle("/css/", http.StripPrefix("/css/", cssHandler))
	http.Handle("/fonts/", http.StripPrefix("/fonts/", fontsHandler))
	http.Handle("/js/", http.StripPrefix("/js/", jsHandler))
	http.Handle("/imgs/", http.StripPrefix("/imgs/", imgsHandler))

	loadTemplates()
	models.InitDB()

	var router = mux.NewRouter()
	router.HandleFunc("/", indexPageHandler)
	router.HandleFunc("/mainList.m3u", getListHandler)

	router.HandleFunc("/counter", showCounterHandler)

	router.HandleFunc("/login", loginHandler).Methods("POST")
	router.HandleFunc("/personales", personalesHandler).Methods("POST")
	router.HandleFunc("/searchWord", searchChannelHanler).Methods("POST")
	router.HandleFunc("/logout", logoutHandler).Methods("POST")
	router.HandleFunc("/actionChannel", actionChannelHandler).Methods("POST")
	router.HandleFunc("/addLink", addLinkHandler).Methods("POST")
	router.HandleFunc("/voteLink", actionLinkHandler).Methods("POST")
	router.HandleFunc("/register", registerHandler).Methods("POST")
	router.HandleFunc("/addChannel", addChannelHandler).Methods("POST")
	//router.NotFoundHandler = http.HandlerFunc(notFoundHandler)

	http.Handle("/", router)
	http.Handle(urlMainList, router)
	//http.ListenAndServe(":"+port, nil)

	//lista := listGenerator.NewList()
	cl, err := models.GetChannelMainList()
	links, err := models.GetLinks(-1)

	if err == nil {
		//Iniciando el crontab que genera lista m3u
		log.Print("Iniciando lista principal y secuencia por hora")
		go listGenerator.GenerateList(mainListFile, cl)
		go listGenerator.CheckListUrls(links)
		c := cron.New()
		c.AddFunc("@hourly", func() {
			cl, err = models.GetChannelMainList()
			listGenerator.GenerateList(mainListFile, cl)
		})
		c.AddFunc("@every 12h0m", func() {
			links, err = models.GetLinks(-1)
			if err == nil {
				listGenerator.CheckListUrls(links)
			} else {
				log.Print(err)
			}
		})
		c.Start()
	} else {
		log.Print(err)
	}

	log.Print(http.ListenAndServe(":"+port, nil))
}
