package main

import (
	"html/template"
	"net/http"
)

var templates = template.Must(template.ParseGlob("templates/*.tmpl"))

func loadTemplates() {
	for _, t := range templates.Templates() {
		//log.Print(fmt.Sprintf("  # %d -> %s", i, t.Name()))
		templates.Lookup(t.Name())
	}
	//templates.Lookup("busqueda-centro.tmpl.html")
	//templates.Lookup("navbar.tmpl.html")
}

func renderTemplate(w http.ResponseWriter, r *http.Request, tmpl string, ctx Context) {
	err := templates.ExecuteTemplate(w, tmpl+".tmpl", ctx)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
