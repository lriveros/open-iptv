package models

//Query para login del usuario
var queryLogin = `SELECT idUser, username, name, COALESCE(middleName,'-') AS middleName, lastName, image, email, status, type, createdAt, lastConnection, birthday, countryCode
				FROM Users WHERE (username = '%s' OR email='%s') AND password = '%s';`

//Query para traer los canales top x
var qTopChannels = "SELECT idChannel, number, name, logo, language, countryCode, description, score FROM Channel ORDER BY score DESC LIMIT %s"

//Query para traer canales por pais e idioma
var qChannelsCountry = "SELECT idChannel, number, name, logo, language, countryCode, description FROM Channel WHERE countryCode='%s'"
var qChannelsLang = "SELECT idChannel, number, name, logo, language, countryCode, description FROM Channel WHERE language='%s'"

//Query para traer los link de un canal
var qChannelLinks = "SELECT idLink, idChannel, url, status, ranking FROM Link WHERE idChannel = %d ORDER BY ranking DESC, status ASC"

//Query para buscar canales
var qChannelsSearch = "SELECT idChannel, number, name, logo, language, countryCode, description, score FROM Channel WHERE name ILIKE '%%%s%%'"
var qLastIDChannel = "SELECT idChannel FROM Channel WHERE name = '%s' ORDER BY idChannel DESC"
var qChannel = "SELECT idChannel, number, name, logo, language, countryCode, description, score FROM Channel WHERE idChannel = %d"

//Query que trae los canales con link activo (solo el link con mayor puntuacion)
var qChnl = `SELECT A.name, A.logo, B.url, B.status, B.ranking
			FROM Channel A, Link B
			WHERE B.idlink IN (SELECT idlink from Link WHERE status=1 AND idchannel =A.idChannel ORDER BY ranking DESC LIMIT 1)
			ORDER BY A.number ASC`

//Query que trae los canales listos para generar la lista
var qChnlLst = `SELECT A.number, A.name, A.logo, B.url, ARRAY_TO_STRING(ARRAY_AGG(D.name),',')
				FROM Channel A, Link B, parCategoryChannel C, Category D
				WHERE B.idlink IN (SELECT idlink from Link WHERE status=1 AND idchannel =A.idChannel ORDER BY ranking DESC LIMIT 1)
				AND A.idChannel = C.idChannel
				AND C.idCategory = D.idCategory
				GROUP BY A.name, A.logo, B.url, A.number
				ORDER BY A.number ASC`

//Query para obtener el listado completo de links
var qActiveLink = "SELECT idLink, idChannel, url, status, ranking FROM Link WHERE status = 1"

//Insert para link de canales
var iChnlLink = "INSERT INTO Link (url, idChannel, ranking, status) VALUES ('%s', %d, 3, 1)"

//Insert de accion sobre link de canal
var iAccionLink = "INSERT INTO parLinkUser(idLink, idUser, idAction, actionDate) VALUES (%d, %d, %d, current_timestamp);"

//Insert de accion sobre evaluacion de canal
var iAcctionChannel = "INSERT INTO parChannelUser(idChannel, idUser, idAction, actionDate) VALUES (%d, %d, %d, current_timestamp);"

//Insert para Usuarios
var iUser = `INSERT INTO Users (username, name, lastName, image, email, password, type, status, createdAt) VALUES
							('%s', '%s', '%s', 'default.png', '%s','%s',1,1,current_timestamp)`

var iChannel = `INSERT INTO Channel (number, name, logo, language, countryCode, description, score) VALUES
									(10000, '%s', '%s', 'esp', 0, '%s', 0)`

var iCatergoryChannel = `INSERT INTO parCategoryChannel (idcategory, idchannel) VALUES (%d,%d)`

//Update para el tiempo de last LastConnection
var uUserLoginTime = "UPDATE Users SET LastConnection = current_timestamp WHERE idUser = %d"

//Update para links
var uLink = "UPDATE Link SET status = %d WHERE idLink = %d"
