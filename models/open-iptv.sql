--SQL SCRIPT PARA POSTGRES

DROP TRIGGER IF EXISTS updateLinkRankingTrigger ON parLinkUser;
DROP TRIGGER IF EXISTS deletePreviousVoteLinkTrigger ON parLinkUser;
DROP TRIGGER IF EXISTS updateChannelScoreTrigger ON parChannelUser;
DROP TRIGGER IF EXISTS deletePreviousVoteTrigger ON parChannelUser;

DROP TABLE IF EXISTS parChannelUser;
DROP TABLE IF EXISTS parLinkUser;
DROP TABLE IF EXISTS Link;
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS parCategoryChannel;
DROP TABLE IF EXISTS Channel;
DROP TABLE IF EXISTS Country;
DROP TABLE IF EXISTS UserType;
DROP TABLE IF EXISTS UserStatus;
DROP TABLE IF EXISTS Category;

-- -----------------------------------------------------
-- Table Category
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS Category (
  idCategory SERIAL NOT NULL,
  name VARCHAR(100) NULL,
  description VARCHAR(200) NULL,
  PRIMARY KEY (idCategory)
 );


-- -----------------------------------------------------
	-- Table UserStatus
-- -----------------------------------------------------
CREATE TABLE UserStatus (
    idUserStatus INT NOT NULL,
    detalle VARCHAR(100),
    PRIMARY KEY (idUserStatus)
);


-- -----------------------------------------------------
	-- Table UserStatus
-- -----------------------------------------------------
CREATE TABLE UserType (
    idUserType INT NOT NULL,
    detalle VARCHAR(100),
    PRIMARY KEY (idUserType)
);

-- -----------------------------------------------------
-- Table Country
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS Country (
  code INT NOT NULL,
  alpha2 VARCHAR(2) NULL,
  alpha3 VARCHAR(3) NULL,
  langCS VARCHAR(45) NULL,
  langDE VARCHAR(45) NULL,
  langEN VARCHAR(45) NULL,
  langES VARCHAR(45) NULL,
  langFR VARCHAR(45) NULL,
  langIT VARCHAR(45) NULL,
  langNL VARCHAR(45) NULL,
  flag VARCHAR(30) NULL,
  PRIMARY KEY (code) );


-- -----------------------------------------------------
-- Table Channel
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS Channel (
  idChannel BIGSERIAL,
  number INT NULL,
  name VARCHAR(100) NULL,
  logo VARCHAR(70) NULL,
  language VARCHAR(50) NULL,
  countryCode INT NULL,
  description VARCHAR(200) NULL,
  score FLOAT NULL,
  PRIMARY KEY (idChannel),
  CONSTRAINT fkCountry
    FOREIGN KEY (countryCode )
    REFERENCES Country (code )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table parCategoryChannel
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS parCategoryChannel (
  idCategory INT NOT NULL,
  idChannel INT NULL,
  PRIMARY KEY (idCategory, idChannel) ,
  CONSTRAINT fk_idCategory
    FOREIGN KEY (idCategory )
    REFERENCES Category (idCategory )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_idChannel
    FOREIGN KEY (idChannel )
    REFERENCES Channel (idChannel )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table User
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS Users (
  idUser BIGSERIAL NOT NULL,
  username VARCHAR(100) NULL UNIQUE,
  name VARCHAR(150) NULL,
  middleName VARCHAR(100) NULL,
  lastName VARCHAR(150) NULL,
  image VARCHAR(150) NULL,
  email VARCHAR(100) NULL,
  password VARCHAR(500) NULL,
  status INT NULL,
  type INT NULL,
  createdAt TIMESTAMP NULL,
  lastConnection TIMESTAMP NULL,
  birthday DATE NULL,
  countryCode INT NULL,
  PRIMARY KEY (idUser) ,
  CONSTRAINT fkUserCountry
    FOREIGN KEY (countryCode )
    REFERENCES Country (code )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fkUserType
    FOREIGN KEY (type )
    REFERENCES UserType (idUserType )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fkUserStatus
    FOREIGN KEY (status )
    REFERENCES UserStatus (idUserStatus )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table Link
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS Link (
  idLink BIGSERIAL NOT NULL,
  url VARCHAR(500) NULL UNIQUE,
  idChannel INT NULL,
  ranking FLOAT NULL,
  status INT NULL,
  PRIMARY KEY (idLink) ,
  CONSTRAINT fk_linkChannel
    FOREIGN KEY (idChannel )
    REFERENCES Channel (idChannel )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table parLinkUser
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS parLinkUser (
  idLink BIGINT NOT NULL,
  idUser BIGINT NOT NULL,
  idAction INT NULL,
  detail VARCHAR(1000) NULL,
  actionDate TIMESTAMP NULL,
  PRIMARY KEY (idLink, idUser, idAction) ,
  CONSTRAINT fk_UserParLU
    FOREIGN KEY (idUser )
    REFERENCES Users (idUser )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_LinkParLU
    FOREIGN KEY (idLink )
    REFERENCES Link (idLink )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

    -- -----------------------------------------------------
-- Table parChannelUser
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS parChannelUser (
  idChannel BIGINT NOT NULL,
  idUser BIGINT NOT NULL,
  idAction INT NULL,
  detail VARCHAR(1000) NULL,
  actionDate TIMESTAMP NULL,
  PRIMARY KEY (idChannel, idUser, idAction) ,
  CONSTRAINT fk_UserParLU
    FOREIGN KEY (idUser )
    REFERENCES Users (idUser )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_ChannelParLU
    FOREIGN KEY (idChannel )
    REFERENCES Channel (idChannel )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


--TRIGGERS

--TRIGGER QUE ACTUALIZA EL RANKING DE UN LINK AL SER EVALUADO
CREATE OR REPLACE FUNCTION updateLinkRanking()
    RETURNS TRIGGER AS
$$
DECLARE
	voteUp FLOAT;
    voteDown FLOAT;
    rkg FLOAT;
BEGIN
    voteUp := (SELECT count(*) FROM parLinkUser WHERE idAction = 2 AND idLink = NEW.idLink);
    voteDown := (SELECT count(*) FROM parLinkUser WHERE idAction = 3 AND idLink = NEW.idLink);
    IF (voteUp + voteDown) > 0 THEN
        rkg := voteUp*100.0 / (voteUp + voteDown);
        UPDATE Link SET ranking = rkg WHERE idLink = NEW.idLink;
    END IF;
    RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER updateLinkRankingTrigger
  AFTER INSERT OR UPDATE
  ON parLinkUser
  FOR EACH ROW
  EXECUTE PROCEDURE updateLinkRanking();

  --TRIGGER QUE ACTUALIZA EL RANKING DE UN LINK AL SER EVALUADO
  CREATE OR REPLACE FUNCTION deletePreviousVoteLink()
            RETURNS TRIGGER AS
        $$
        DECLARE
        BEGIN
            DELETE FROM parLinkUser WHERE idUser = NEW.idUser AND idLink = NEW.idLink AND idAction IN (2,3);
            RETURN NEW;
        END;
        $$
        LANGUAGE 'plpgsql';

  CREATE TRIGGER deletePreviousVoteLinkTrigger
    BEFORE INSERT OR UPDATE
    ON parLinkUser
    FOR EACH ROW
    EXECUTE PROCEDURE deletePreviousVoteLink();

--TRIGGER QUE ACTUALIZA EL SCORE DE UN CANAL AL SER EVALUADO
CREATE OR REPLACE FUNCTION updateChannelScore()
      RETURNS TRIGGER AS
  $$
  DECLARE
  	voteUp FLOAT;
      voteDown FLOAT;
      rkg FLOAT;
  BEGIN
      voteUp := (SELECT count(*) FROM parChannelUser WHERE idAction = 2 AND idChannel = NEW.idChannel);
      voteDown := (SELECT count(*) FROM parChannelUser WHERE idAction = 3 AND idChannel = NEW.idChannel);
      IF (voteUp + voteDown) > 0 THEN
          rkg := voteUp*100.0 / (voteUp + voteDown);
          UPDATE Channel SET Score = rkg WHERE idChannel = NEW.idChannel;
      END IF;
      RETURN NEW;
  END;
  $$
  LANGUAGE 'plpgsql';

CREATE TRIGGER updateChannelScoreTrigger
    AFTER INSERT OR UPDATE
    ON parChannelUser
    FOR EACH ROW
    EXECUTE PROCEDURE updateChannelScore();


--TRIGGER QUE ACTUALIZA EL RANKING DE UN LINK AL SER EVALUADO
CREATE OR REPLACE FUNCTION deletePreviousVote()
          RETURNS TRIGGER AS
      $$
      DECLARE
      BEGIN
          DELETE FROM parChannelUser WHERE idUser = NEW.idUser AND idChannel = NEW.idChannel AND idAction IN (2,3);
          RETURN NEW;
      END;
      $$
      LANGUAGE 'plpgsql';

CREATE TRIGGER deletePreviousVoteTrigger
  BEFORE INSERT OR UPDATE
  ON parChannelUser
  FOR EACH ROW
  EXECUTE PROCEDURE deletePreviousVote();

--INSERT BASICOS
-- -----------------------------------------------------
-- Data for table UserStatus
-- -----------------------------------------------------
INSERT INTO UserStatus (idUserStatus, detalle) VALUES (0,'Inactivo'),
(1,'Por Validar'),
(2,'Activo'),
(3,'Eliminado'),
(4,'Bloqueado');

-- -----------------------------------------------------
-- Data for table UserType
-- -----------------------------------------------------
INSERT INTO UserType (idUserType, detalle) VALUES (1,'Usuario'),
(2,'Mantención'),
(3,'Administrador');

-- -----------------------------------------------------
-- Data for table Category
-- -----------------------------------------------------
INSERT INTO Category (idCategory, name, description) VALUES (0, 'None', 'Sin cateoria'),
(1, 'Kids', 'Categoria de Niños'),
(2, 'Regional', 'Canales nacionales'),
(3, 'Commerce', 'Canales de comercio'),
(4, 'World', 'Canales globales'),
(5, 'Premium', 'Canales Premium'),
(6, 'Music', 'Canales relacionadas al mundo de la música'),
(7, 'Sports', 'Canales deportivos'),
(8, 'News', 'Canales de noticias'),
(9, 'Variety', NULL),
(10, 'Entertaiment', NULL),
(11, 'Movies', NULL),
(12, 'Cultural', NULL),
(13, 'Music', NULL),
(14, 'Networks', NULL),
(15, 'HD Channels', NULL);

-- -----------------------------------------------------
-- Data for table  Country
-- -----------------------------------------------------
INSERT INTO Country (code, alpha2, alpha3, langCS, langDE, langEN, langES, langFR, langIT, langNL) VALUES
(0, 'UN', 'UNK', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown'),
(4, 'AF', 'AFG', 'Afghanistán', 'Afghanistan', 'Afghanistan', 'Afganistán', 'Afghanistan', 'Afghanistan', 'Afghanistan'),
(8, 'AL', 'ALB', 'Albánie', 'Albanien', 'Albania', 'Albania', 'Albanie', 'Albania', 'Albanië'),
(10, 'AQ', 'ATA', 'Antarctica', 'Antarktis', 'Antarctica', 'Antartida', 'Antarctique', 'Antartide', 'Antarctica'),
(12, 'DZ', 'DZA', 'Alžírsko', 'Algerien', 'Algeria', 'Argelia', 'Algérie', 'Algeria', 'Algerije'),
(16, 'AS', 'ASM', 'Americká Samoa', 'Amerikanisch-Samoa', 'American Samoa', 'Samoa americana', 'Samoa Américaines', 'Samoa Americane', 'Amerikaans Samoa'),
(20, 'AD', 'AND', 'Andorra', 'Andorra', 'Andorra', 'Andorra', 'Andorre', 'Andorra', 'Andorra'),
(24, 'AO', 'AGO', 'Angola', 'Angola', 'Angola', 'Angola', 'Angola', 'Angola', 'Angola'),
(28, 'AG', 'ATG', 'Antigua a Barbuda', 'Antigua und Barbuda', 'Antigua and Barbuda', 'Antigua y Barbuda', 'Antigua-et-Barbuda', 'Antigua e Barbuda', 'Antigua en Barbuda'),
(31, 'AZ', 'AZE', 'Azerbajdžán', 'Aserbaidschan', 'Azerbaijan', 'Azerbaiyán', 'Azerbaïdjan', 'Azerbaijan', 'Azerbeidzjan'),
(32, 'AR', 'ARG', 'Argentina', 'Argentinien', 'Argentina', 'Argentina', 'Argentine', 'Argentina', 'Argentinië'),
(36, 'AU', 'AUS', 'Austrálie', 'Australien', 'Australia', 'Australia', 'Australie', 'Australia', 'Australië'),
(40, 'AT', 'AUT', 'Rakousko', 'Österreich', 'Austria', 'Austria', 'Autriche', 'Austria', 'Oostenrijk'),
(44, 'BS', 'BHS', 'Bahamy', 'Bahamas', 'Bahamas', 'Bahamas', 'Bahamas', 'Bahamas', 'Bahama''s'),
(48, 'BH', 'BHR', 'Bahrajn', 'Bahrain', 'Bahrain', 'Bahrain', 'Bahreïn', 'Bahrain', 'Bahrein'),
(50, 'BD', 'BGD', 'Bangladéš', 'Bangladesch', 'Bangladesh', 'Bangladesh', 'Bangladesh', 'Bangladesh', 'Bangladesh'),
(51, 'AM', 'ARM', 'Arménie', 'Armenien', 'Armenia', 'Armenia', 'Arménie', 'Armenia', 'Armenië'),
(52, 'BB', 'BRB', 'Barbados', 'Barbados', 'Barbados', 'Barbados', 'Barbade', 'Barbados', 'Barbados'),
(56, 'BE', 'BEL', 'Belgie', 'Belgien', 'Belgium', 'Bélgica', 'Belgique', 'Belgio', 'België'),
(60, 'BM', 'BMU', 'Bermuda', 'Bermuda', 'Bermuda', 'Bermuda', 'Bermudes', 'Bermuda', 'Bermuda'),
(64, 'BT', 'BTN', 'Bhután', 'Bhutan', 'Bhutan', 'Bhutan', 'Bhoutan', 'Bhutan', 'Bhutan'),
(68, 'BO', 'BOL', 'Bolívie', 'Bolivien', 'Bolivia', 'Bolivia', 'Bolivie', 'Bolivia', 'Bolivia'),
(70, 'BA', 'BIH', 'Bosna a Hercegovina', 'Bosnien und Herzegowina', 'Bosnia and Herzegovina', 'Bosnia y Herzegovina', 'Bosnie-Herzégovine', 'Bosnia Erzegovina', 'Bosnië-Herzegovina'),
(72, 'BW', 'BWA', 'Botswana', 'Botswana', 'Botswana', 'Botswana', 'Botswana', 'Botswana', 'Botswana'),
(74, 'BV', 'BVT', 'Bouvet Island', 'Bouvetinsel', 'Bouvet Island', 'Isla Bouvet', 'Île Bouvet', 'Isola di Bouvet', 'Bouvet'),
(76, 'BR', 'BRA', 'Brazílie', 'Brasilien', 'Brazil', 'Brasil', 'Brésil', 'Brasile', 'Brazilië'),
(84, 'BZ', 'BLZ', 'Belize', 'Belize', 'Belize', 'Belize', 'Belize', 'Belize', 'Belize'),
(86, 'IO', 'IOT', 'Britské Indickooceánské teritorium', 'Britisches Territorium im Indischen Ozean', 'British Indian Ocean Territory', 'Territorio Oceánico de la India Británica', 'Territoire Britannique de l''Océan Indien', 'Territori Britannici dell''Oceano Indiano', 'British Indian Ocean Territory'),
(90, 'SB', 'SLB', 'Šalamounovy ostrovy', 'Salomonen', 'Solomon Islands', 'Islas Salomón', 'Îles Salomon', 'Isole Solomon', 'Salomonseilanden'),
(92, 'VG', 'VGB', 'Britské Panenské ostrovy', 'Britische Jungferninseln', 'British Virgin Islands', 'Islas Vírgenes Británicas', 'Îles Vierges Britanniques', 'Isole Vergini Britanniche', 'Britse Maagdeneilanden'),
(96, 'BN', 'BRN', 'Brunej', 'Brunei Darussalam', 'Brunei Darussalam', 'Brunei Darussalam', 'Brunéi Darussalam', 'Brunei Darussalam', 'Brunei'),
(100, 'BG', 'BGR', 'Bulharsko', 'Bulgarien', 'Bulgaria', 'Bulgaria', 'Bulgarie', 'Bulgaria', 'Bulgarije'),
(104, 'MM', 'MMR', 'Myanmar', 'Myanmar', 'Myanmar', 'Mianmar', 'Myanmar', 'Myanmar', 'Myanmar'),
(108, 'BI', 'BDI', 'Burundi', 'Burundi', 'Burundi', 'Burundi', 'Burundi', 'Burundi', 'Burundi'),
(112, 'BY', 'BLR', 'Bělorusko', 'Belarus', 'Belarus', 'Belarus', 'Bélarus', 'Bielorussia', 'Wit-Rusland'),
(116, 'KH', 'KHM', 'Kambodža', 'Kambodscha', 'Cambodia', 'Camboya', 'Cambodge', 'Cambogia', 'Cambodja'),
(120, 'CM', 'CMR', 'Kamerun', 'Kamerun', 'Cameroon', 'Camerún', 'Cameroun', 'Camerun', 'Kameroen'),
(124, 'CA', 'CAN', 'Kanada', 'Kanada', 'Canada', 'Canadá', 'Canada', 'Canada', 'Canada'),
(132, 'CV', 'CPV', 'Ostrovy Zeleného mysu', 'Kap Verde', 'Cape Verde', 'Cabo Verde', 'Cap-vert', 'Capo Verde', 'Kaapverdië'),
(136, 'KY', 'CYM', 'Kajmanské ostrovy', 'Kaimaninseln', 'Cayman Islands', 'Islas Caimán', 'Îles Caïmanes', 'Isole Cayman', 'Caymaneilanden'),
(140, 'CF', 'CAF', 'Středoafrická republika', 'Zentralafrikanische Republik', 'Central African', 'República Centroafricana', 'République Centrafricaine', 'Repubblica Centroafricana', 'Centraal-Afrikaanse Republiek'),
(144, 'LK', 'LKA', 'Srí Lanka', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka'),
(148, 'TD', 'TCD', 'Čad', 'Tschad', 'Chad', 'Chad', 'Tchad', 'Ciad', 'Tsjaad'),
(152, 'CL', 'CHL', 'Chile', 'Chile', 'Chile', 'Chile', 'Chili', 'Cile', 'Chili'),
(156, 'CN', 'CHN', 'Čína', 'China', 'China', 'China', 'Chine', 'Cina', 'China'),
(158, 'TW', 'TWN', 'Tchajwan', 'Taiwan', 'Taiwan', 'Taiwán', 'Taïwan', 'Taiwan', 'Taiwan'),
(162, 'CX', 'CXR', 'Christmas Island', 'Weihnachtsinsel', 'Christmas Island', 'Isla Navidad', 'Île Christmas', 'Isola di Natale', 'Christmaseiland'),
(166, 'CC', 'CCK', 'Kokosové ostrovy', 'Kokosinseln', 'Cocos (Keeling) Islands', 'Islas Cocos (Keeling)', 'Îles Cocos (Keeling)', 'Isole Cocos', 'Cocoseilanden'),
(170, 'CO', 'COL', 'Kolumbie', 'Kolumbien', 'Colombia', 'Colombia', 'Colombie', 'Colombia', 'Colombia'),
(174, 'KM', 'COM', 'Komory', 'Komoren', 'Comoros', 'Comoros', 'Comores', 'Comore', 'Comoren'),
(175, 'YT', 'MYT', 'Mayotte', 'Mayotte', 'Mayotte', 'Mayote', 'Mayotte', 'Mayotte', 'Mayotte'),
(178, 'CG', 'COG', 'Konžská republika Kongo', 'Republik Kongo', 'Republic of the Congo', 'Congo', 'République du Congo', 'Repubblica del Congo', 'Republiek Congo'),
(180, 'CD', 'COD', 'Demokratická republika Kongo Kongo', 'Demokratische Republik Kongo', 'The Democratic Republic Of The Congo', 'República Democrática del Congo', 'République Démocratique du Congo', 'Repubblica Democratica del Congo', 'Democratische Republiek Congo'),
(184, 'CK', 'COK', 'Cookovy ostrovy', 'Cookinseln', 'Cook Islands', 'Islas Cook', 'Îles Cook', 'Isole Cook', 'Cookeilanden'),
(188, 'CR', 'CRI', 'Kostarika', 'Costa Rica', 'Costa Rica', 'Costa Rica', 'Costa Rica', 'Costa Rica', 'Costa Rica'),
(191, 'HR', 'HRV', 'Chorvatsko', 'Kroatien', 'Croatia', 'Croacia', 'Croatie', 'Croazia', 'Kroatië'),
(192, 'CU', 'CUB', 'Kuba', 'Kuba', 'Cuba', 'Cuba', 'Cuba', 'Cuba', 'Cuba'),
(196, 'CY', 'CYP', 'Kypr', 'Zypern', 'Cyprus', 'Chipre', 'Chypre', 'Cipro', 'Cyprus'),
(203, 'CZ', 'CZE', 'Česko', 'Tschechische Republik', 'Czech Republic', 'Chequia', 'République Tchèque', 'Repubblica Ceca', 'Tsjechië'),
(204, 'BJ', 'BEN', 'Benin', 'Benin', 'Benin', 'Benin', 'Bénin', 'Benin', 'Benin'),
(208, 'DK', 'DNK', 'Dánsko', 'Dänemark', 'Denmark', 'Dinamarca', 'Danemark', 'Danimarca', 'Denemarken'),
(212, 'DM', 'DMA', 'Dominika', 'Dominica', 'Dominica', 'Dominica', 'Dominique', 'Dominica', 'Dominica'),
(214, 'DO', 'DOM', 'Dominikánská republika', 'Dominikanische Republik', 'Dominican Republic', 'República Dominicana', 'République Dominicaine', 'Repubblica Dominicana', 'Dominicaanse Republiek'),
(218, 'EC', 'ECU', 'Ekvádor', 'Ecuador', 'Ecuador', 'Ecuador', 'Équateur', 'Ecuador', 'Ecuador'),
(222, 'SV', 'SLV', 'Salvador', 'El Salvador', 'El Salvador', 'El Salvador', 'El Salvador', 'El Salvador', 'El Salvador'),
(226, 'GQ', 'GNQ', 'Rovníková Guinea', 'Äquatorialguinea', 'Equatorial Guinea', 'Guinea Ecuatorial', 'Guinée Équatoriale', 'Guinea Equatoriale', 'Equatoriaal Guinea'),
(231, 'ET', 'ETH', 'Etiopie', 'Äthiopien', 'Ethiopia', 'Etiopía', 'Éthiopie', 'Etiopia', 'Ethiopië'),
(232, 'ER', 'ERI', 'Eritrea', 'Eritrea', 'Eritrea', 'Eritrea', 'Érythrée', 'Eritrea', 'Eritrea'),
(233, 'EE', 'EST', 'Estonsko', 'Estland', 'Estonia', 'Estonia', 'Estonie', 'Estonia', 'Estland'),
(234, 'FO', 'FRO', 'Faerské ostrovy', 'Färöer', 'Faroe Islands', 'Islas Faroe', 'Îles Féroé', 'Isole Faroe', 'Faeröer'),
(238, 'FK', 'FLK', 'Falklandské ostrovy', 'Falklandinseln', 'Falkland Islands', 'Islas Malvinas', 'Îles (malvinas) Falkland', 'Isole Falkland', 'Falklandeilanden'),
(239, 'GS', 'SGS', 'Jižní Georgie a Jižní Sandwichovy ostrovy', 'Südgeorgien und die Südlichen Sandwichinseln', 'South Georgia and the South Sandwich Islands', 'Georgia del Sur e Islas Sandwich del Sur', 'Géorgie du Sud et les Îles Sandwich du Sud', 'Sud Georgia e Isole Sandwich', 'Zuid-Georgië en de Zuidelijke Sandwicheilande'),
(242, 'FJ', 'FJI', 'Fidži', 'Fidschi', 'Fiji', 'Fiji', 'Fidji', 'Fiji', 'Fiji'),
(246, 'FI', 'FIN', 'Finsko', 'Finnland', 'Finland', 'Finlandia', 'Finlande', 'Finlandia', 'Finland'),
(248, 'AX', 'ALA', 'Åland Islands', 'Åland-Inseln', 'Åland Islands', 'IslasÅland', 'Îles Åland', 'Åland Islands', 'Åland Islands'),
(250, 'FR', 'FRA', 'Francie', 'Frankreich', 'France', 'Francia', 'France', 'Francia', 'Frankrijk'),
(254, 'GF', 'GUF', 'Francouzská Guayana', 'Französisch-Guayana', 'French Guiana', 'Guinea Francesa', 'Guyane Française', 'Guyana Francese', 'Frans-Guyana'),
(258, 'PF', 'PYF', 'Francouzská Polynésie', 'Französisch-Polynesien', 'French Polynesia', 'Polinesia Francesa', 'Polynésie Française', 'Polinesia Francese', 'Frans-Polynesië'),
(260, 'TF', 'ATF', 'Francouzská jižní teritoria', 'Französische Süd- und Antarktisgebiete', 'French Southern Territories', 'Territorios Sureños de Francia', 'Terres Australes Françaises', 'Territori Francesi del Sud', 'Franse Zuidelijke en Antarctische gebieden'),
(262, 'DJ', 'DJI', 'Džibutsko', 'Dschibuti', 'Djibouti', 'Djibouti', 'Djibouti', 'Gibuti', 'Djibouti'),
(266, 'GA', 'GAB', 'Gabon', 'Gabun', 'Gabon', 'Gabón', 'Gabon', 'Gabon', 'Gabon'),
(268, 'GE', 'GEO', 'Gruzínsko', 'Georgien', 'Georgia', 'Georgia', 'Géorgie', 'Georgia', 'Georgië'),
(270, 'GM', 'GMB', 'Gambie', 'Gambia', 'Gambia', 'Gambia', 'Gambie', 'Gambia', 'Gambia'),
(275, 'PS', 'PSE', 'Palestinská území', 'Palästinensische Autonomiegebiete', 'Occupied Palestinian Territory', 'Palestina', 'Territoire Palestinien Occupé', 'Territori Palestinesi Occupati', 'Palestina'),
(276, 'DE', 'DEU', 'Německo', 'Deutschland', 'Germany', 'Alemania', 'Allemagne', 'Germania', 'Duitsland'),
(288, 'GH', 'GHA', 'Ghana', 'Ghana', 'Ghana', 'Ghana', 'Ghana', 'Ghana', 'Ghana'),
(292, 'GI', 'GIB', 'Gibraltar', 'Gibraltar', 'Gibraltar', 'Gibraltar', 'Gibraltar', 'Gibilterra', 'Gibraltar'),
(296, 'KI', 'KIR', 'Kiribati', 'Kiribati', 'Kiribati', 'Kiribati', 'Kiribati', 'Kiribati', 'Kiribati'),
(300, 'GR', 'GRC', 'Řecko', 'Griechenland', 'Greece', 'Grecia', 'Grèce', 'Grecia', 'Griekenland'),
(304, 'GL', 'GRL', 'Grónsko', 'Grönland', 'Greenland', 'Groenlandia', 'Groenland', 'Groenlandia', 'Groenland'),
(308, 'GD', 'GRD', 'Grenada', 'Grenada', 'Grenada', 'Granada', 'Grenade', 'Grenada', 'Grenada'),
(312, 'GP', 'GLP', 'Guadeloupe', 'Guadeloupe', 'Guadeloupe', 'Guadalupe', 'Guadeloupe', 'Guadalupa', 'Guadeloupe'),
(316, 'GU', 'GUM', 'Guam', 'Guam', 'Guam', 'Guam', 'Guam', 'Guam', 'Guam'),
(320, 'GT', 'GTM', 'Guatemala', 'Guatemala', 'Guatemala', 'Guatemala', 'Guatemala', 'Guatemala', 'Guatemala'),
(324, 'GN', 'GIN', 'Guinea', 'Guinea', 'Guinea', 'Guinea', 'Guinée', 'Guinea', 'Guinee'),
(328, 'GY', 'GUY', 'Guyana', 'Guyana', 'Guyana', 'Guayana', 'Guyana', 'Guyana', 'Guyana'),
(332, 'HT', 'HTI', 'Haiti', 'Haiti', 'Haiti', 'Haití', 'Haïti', 'Haiti', 'Haiti'),
(334, 'HM', 'HMD', 'Heardův ostrov a McDonaldovy ostrovy', 'Heard und McDonaldinseln', 'Heard Island and McDonald Islands', 'Islas Heard e Islas McDonald', 'Îles Heard et Mcdonald', 'Isola Heard e Isole McDonald', 'Heard- en McDonaldeilanden'),
(336, 'VA', 'VAT', 'Vatikán', 'Vatikanstadt', 'Vatican City State', 'Estado Vaticano', 'Saint-Siège (état de la Cité du Vatican)', 'Città del Vaticano', 'Vaticaanstad'),
(340, 'HN', 'HND', 'Honduras', 'Honduras', 'Honduras', 'Honduras', 'Honduras', 'Honduras', 'Honduras'),
(344, 'HK', 'HKG', 'Hong Kong', 'Hongkong', 'Hong Kong', 'Hong Kong', 'Hong-Kong', 'Hong Kong', 'Hongkong'),
(348, 'HU', 'HUN', 'Maďarsko', 'Ungarn', 'Hungary', 'Hungría', 'Hongrie', 'Ungheria', 'Hongarije'),
(352, 'IS', 'ISL', 'Island', 'Island', 'Iceland', 'Islandia', 'Islande', 'Islanda', 'IJsland'),
(356, 'IN', 'IND', 'Indie', 'Indien', 'India', 'India', 'Inde', 'India', 'India'),
(360, 'ID', 'IDN', 'Indonésie', 'Indonesien', 'Indonesia', 'Indonesia', 'Indonésie', 'Indonesia', 'Indonesië'),
(364, 'IR', 'IRN', 'Írán', 'Islamische Republik Iran', 'Islamic Republic of Iran', 'Irán', 'République Islamique d''Iran', 'Iran', 'Iran'),
(368, 'IQ', 'IRQ', 'Irák', 'Irak', 'Iraq', 'Irak', 'Iraq', 'Iraq', 'Irak'),
(372, 'IE', 'IRL', 'Irsko', 'Irland', 'Ireland', 'Irlanda', 'Irlande', 'Eire', 'Ierland'),
(376, 'IL', 'ISR', 'Izrael', 'Israel', 'Israel', 'Israel', 'Israël', 'Israele', 'Israël'),
(380, 'IT', 'ITA', 'Itálie', 'Italien', 'Italy', 'Italia', 'Italie', 'Italia', 'Italië'),
(384, 'CI', 'CIV', 'Pobřeží slonoviny', 'Côte d''Ivoire', 'Côte d''Ivoire', 'Costa de Marfil', 'Côte d''Ivoire', 'Costa d''Avorio', 'Ivoorkust'),
(388, 'JM', 'JAM', 'Jamajka', 'Jamaika', 'Jamaica', 'Jamaica', 'Jamaïque', 'Giamaica', 'Jamaica'),
(392, 'JP', 'JPN', 'Japonsko', 'Japan', 'Japan', 'Japón', 'Japon', 'Giappone', 'Japan'),
(398, 'KZ', 'KAZ', 'Kazachstán', 'Kasachstan', 'Kazakhstan', 'Kazajstán', 'Kazakhstan', 'Kazakhistan', 'Kazachstan'),
(400, 'JO', 'JOR', 'Jordánsko', 'Jordanien', 'Jordan', 'Jordania', 'Jordanie', 'Giordania', 'Jordanië'),
(404, 'KE', 'KEN', 'Keňa', 'Kenia', 'Kenya', 'Kenia', 'Kenya', 'Kenya', 'Kenia'),
(408, 'KP', 'PRK', 'Severní Korea', 'Demokratische Volksrepublik Korea', 'Democratic People''s Republic of Korea', 'Corea', 'République Populaire Démocratique de Corée', 'Corea del Nord', 'Noord-Korea'),
(410, 'KR', 'KOR', 'Jižní Korea', 'Republik Korea', 'Republic of Korea', 'Corea', 'République de Corée', 'Corea del Sud', 'Zuid-Korea'),
(414, 'KW', 'KWT', 'Kuvajt', 'Kuwait', 'Kuwait', 'Kuwait', 'Koweït', 'Kuwait', 'Koeweit'),
(417, 'KG', 'KGZ', 'Kyrgyzstán', 'Kirgisistan', 'Kyrgyzstan', 'Kirgistán', 'Kirghizistan', 'Kirghizistan', 'Kirgizië'),
(418, 'LA', 'LAO', 'Laos', 'Demokratische Volksrepublik Laos', 'Lao People''s Democratic Republic', 'Laos', 'République Démocratique Populaire Lao', 'Laos', 'Laos'),
(422, 'LB', 'LBN', 'Libanon', 'Libanon', 'Lebanon', 'Líbano', 'Liban', 'Libano', 'Libanon'),
(426, 'LS', 'LSO', 'Lesotho', 'Lesotho', 'Lesotho', 'Lesoto', 'Lesotho', 'Lesotho', 'Lesotho'),
(428, 'LV', 'LVA', 'Lotyšsko', 'Lettland', 'Latvia', 'Letonia', 'Lettonie', 'Lettonia', 'Letland'),
(430, 'LR', 'LBR', 'Libérie', 'Liberia', 'Liberia', 'Liberia', 'Libéria', 'Liberia', 'Liberia'),
(434, 'LY', 'LBY', 'Libye', 'Libysch-Arabische Dschamahirija', 'Libyan Arab Jamahiriya', 'Libia', 'Jamahiriya Arabe Libyenne', 'Libia', 'Libië'),
(438, 'LI', 'LIE', 'Lichtenštejnsko', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein'),
(440, 'LT', 'LTU', 'Litva', 'Litauen', 'Lithuania', 'Lituania', 'Lituanie', 'Lituania', 'Litouwen'),
(442, 'LU', 'LUX', 'Lucembursko', 'Luxemburg', 'Luxembourg', 'Luxemburgo', 'Luxembourg', 'Lussemburgo', 'Groothertogdom Luxemburg'),
(446, 'MO', 'MAC', 'Macao', 'Macao', 'Macao', 'Macao', 'Macao', 'Macao', 'Macao'),
(450, 'MG', 'MDG', 'Madagaskar', 'Madagaskar', 'Madagascar', 'Madagascar', 'Madagascar', 'Madagascar', 'Madagaskar'),
(454, 'MW', 'MWI', 'Malawi', 'Malawi', 'Malawi', 'Malawi', 'Malawi', 'Malawi', 'Malawi'),
(458, 'MY', 'MYS', 'Malajsie', 'Malaysia', 'Malaysia', 'Malasia', 'Malaisie', 'Malesia', 'Maleisië'),
(462, 'MV', 'MDV', 'Maledivy', 'Malediven', 'Maldives', 'Maldivas', 'Maldives', 'Maldive', 'Maldiven'),
(466, 'ML', 'MLI', 'Mali', 'Mali', 'Mali', 'Mali', 'Mali', 'Mali', 'Mali'),
(470, 'MT', 'MLT', 'Malta', 'Malta', 'Malta', 'Malta', 'Malte', 'Malta', 'Malta'),
(474, 'MQ', 'MTQ', 'Martinik', 'Martinique', 'Martinique', 'Martinica', 'Martinique', 'Martinica', 'Martinique'),
(478, 'MR', 'MRT', 'Mauretánie', 'Mauretanien', 'Mauritania', 'Mauritania', 'Mauritanie', 'Mauritania', 'Mauritanië'),
(480, 'MU', 'MUS', 'Mauricius', 'Mauritius', 'Mauritius', 'Mauricio', 'Maurice', 'Maurizius', 'Mauritius'),
(484, 'MX', 'MEX', 'Mexiko', 'Mexiko', 'Mexico', 'México', 'Mexique', 'Messico', 'Mexico'),
(492, 'MC', 'MCO', 'Monako', 'Monaco', 'Monaco', 'Mónaco', 'Monaco', 'Monaco', 'Monaco'),
(496, 'MN', 'MNG', 'Mongolsko', 'Mongolei', 'Mongolia', 'Mongolia', 'Mongolie', 'Mongolia', 'Mongolië'),
(498, 'MD', 'MDA', 'Moldavsko', 'Moldawien', 'Republic of Moldova', 'Moldavia', 'République de Moldova', 'Moldavia', 'Republiek Moldavië'),
(500, 'MS', 'MSR', 'Montserrat', 'Montserrat', 'Montserrat', 'Montserrat', 'Montserrat', 'Montserrat', 'Montserrat'),
(504, 'MA', 'MAR', 'Maroko', 'Marokko', 'Morocco', 'Marruecos', 'Maroc', 'Marocco', 'Marokko'),
(508, 'MZ', 'MOZ', 'Mosambik', 'Mosambik', 'Mozambique', 'Mozambique', 'Mozambique', 'Mozambico', 'Mozambique'),
(512, 'OM', 'OMN', 'Omán', 'Oman', 'Oman', 'Omán', 'Oman', 'Oman', 'Oman'),
(516, 'NA', 'NAM', 'Namíbie', 'Namibia', 'Namibia', 'Namibia', 'Namibie', 'Namibia', 'Namibië'),
(520, 'NR', 'NRU', 'Nauru', 'Nauru', 'Nauru', 'Nauru', 'Nauru', 'Nauru', 'Nauru'),
(524, 'NP', 'NPL', 'Nepál', 'Nepal', 'Nepal', 'Nepal', 'Népal', 'Nepal', 'Nepal'),
(528, 'NL', 'NLD', 'Nizozemsko', 'Niederlande', 'Netherlands', 'Holanda', 'Pays-Bas', 'Paesi Bassi', 'Nederland'),
(530, 'AN', 'ANT', 'Nizozemské Antily', 'Niederländische Antillen', 'Netherlands Antilles', 'Antillas Holandesas', 'Antilles Néerlandaises', 'Antille Olandesi', 'Nederlandse Antillen'),
(533, 'AW', 'ABW', 'Aruba', 'Aruba', 'Aruba', 'Aruba', 'Aruba', 'Aruba', 'Aruba'),
(540, 'NC', 'NCL', 'Nová Kaledonie', 'Neukaledonien', 'New Caledonia', 'Nueva Caledonia', 'Nouvelle-Calédonie', 'Nuova Caledonia', 'Nieuw-Caledonië'),
(548, 'VU', 'VUT', 'Vanuatu', 'Vanuatu', 'Vanuatu', 'Vanuatu', 'Vanuatu', 'Vanuatu', 'Vanuatu'),
(554, 'NZ', 'NZL', 'Nový Zéland', 'Neuseeland', 'New Zealand', 'Nueva Zelanda', 'Nouvelle-Zélande', 'Nuova Zelanda', 'Nieuw-Zeeland'),
(558, 'NI', 'NIC', 'Nikaragua', 'Nicaragua', 'Nicaragua', 'Nicaragua', 'Nicaragua', 'Nicaragua', 'Nicaragua'),
(562, 'NE', 'NER', 'Niger', 'Niger', 'Niger', 'Níger', 'Niger', 'Niger', 'Niger'),
(566, 'NG', 'NGA', 'Nigérie', 'Nigeria', 'Nigeria', 'Nigeria', 'Nigéria', 'Nigeria', 'Nigeria'),
(570, 'NU', 'NIU', 'Niue', 'Niue', 'Niue', 'Niue', 'Niué', 'Niue', 'Niue'),
(574, 'NF', 'NFK', 'Norfolk Island', 'Norfolkinsel', 'Norfolk Island', 'Islas Norfolk', 'Île Norfolk', 'Isola Norfolk', 'Norfolkeiland'),
(578, 'NO', 'NOR', 'Norsko', 'Norwegen', 'Norway', 'Noruega', 'Norvège', 'Norvegia', 'Noorwegen'),
(580, 'MP', 'MNP', 'Severomariánské ostrovy', 'Nördliche Marianen', 'Northern Mariana Islands', 'Islas de Norte-Mariana', 'Îles Mariannes du Nord', 'Isole Marianne Settentrionali', 'Noordelijke Marianen'),
(581, 'UM', 'UMI', 'United States Minor Outlying Islands', 'Amerikanisch-Ozeanien', 'United States Minor Outlying Islands', 'Islas Ultramarinas de Estados Unidos', 'Îles Mineures Éloignées des États-Unis', 'Isole Minori degli Stati Uniti d''America', 'United States Minor Outlying Eilanden'),
(583, 'FM', 'FSM', 'Mikronésie', 'Mikronesien', 'Federated States of Micronesia', 'Micronesia', 'États Fédérés de Micronésie', 'Stati Federati della Micronesia', 'Micronesië'),
(584, 'MH', 'MHL', 'Marshallovy ostrovy', 'Marshallinseln', 'Marshall Islands', 'Islas Marshall', 'Îles Marshall', 'Isole Marshall', 'Marshalleilanden'),
(585, 'PW', 'PLW', 'Palau', 'Palau', 'Palau', 'Palau', 'Palaos', 'Palau', 'Palau'),
(586, 'PK', 'PAK', 'Pakistán', 'Pakistan', 'Pakistan', 'Pakistán', 'Pakistan', 'Pakistan', 'Pakistan'),
(591, 'PA', 'PAN', 'Panama', 'Panama', 'Panama', 'Panamá', 'Panama', 'Panamá', 'Panama'),
(598, 'PG', 'PNG', 'Papua Nová Guinea', 'Papua-Neuguinea', 'Papua New Guinea', 'Papúa Nueva Guinea', 'Papouasie-Nouvelle-Guinée', 'Papua Nuova Guinea', 'Papoea-Nieuw-Guinea'),
(600, 'PY', 'PRY', 'Paraguay', 'Paraguay', 'Paraguay', 'Paraguay', 'Paraguay', 'Paraguay', 'Paraguay'),
(604, 'PE', 'PER', 'Peru', 'Peru', 'Peru', 'Perú', 'Pérou', 'Perù', 'Peru'),
(608, 'PH', 'PHL', 'Filipíny', 'Philippinen', 'Philippines', 'Filipinas', 'Philippines', 'Filippine', 'Filippijnen'),
(612, 'PN', 'PCN', 'Pitcairn', 'Pitcairninseln', 'Pitcairn', 'Pitcairn', 'Pitcairn', 'Pitcairn', 'Pitcairneilanden'),
(616, 'PL', 'POL', 'Polsko', 'Polen', 'Poland', 'Polonia', 'Pologne', 'Polonia', 'Polen'),
(620, 'PT', 'PRT', 'Portugalsko', 'Portugal', 'Portugal', 'Portugal', 'Portugal', 'Portogallo', 'Portugal'),
(624, 'GW', 'GNB', 'Guinea-Bissau', 'Guinea-Bissau', 'Guinea-Bissau', 'Guinea-Bissau', 'Guinée-Bissau', 'Guinea-Bissau', 'Guinee-Bissau'),
(626, 'TL', 'TLS', 'Východní Timor', 'Timor-Leste', 'Timor-Leste', 'Timor Leste', 'Timor-Leste', 'Timor Est', 'Oost-Timor'),
(630, 'PR', 'PRI', 'Portoriko', 'Puerto Rico', 'Puerto Rico', 'Puerto Rico', 'Porto Rico', 'Porto Rico', 'Puerto Rico'),
(634, 'QA', 'QAT', 'Katar', 'Katar', 'Qatar', 'Qatar', 'Qatar', 'Qatar', 'Qatar'),
(638, 'RE', 'REU', 'Reunion', 'Réunion', 'Réunion', 'Reunión', 'Réunion', 'Reunion', 'Réunion'),
(642, 'RO', 'ROU', 'Rumunsko', 'Rumänien', 'Romania', 'Rumanía', 'Roumanie', 'Romania', 'Roemenië'),
(643, 'RU', 'RUS', 'Rusko', 'Russische Föderation', 'Russian Federation', 'Rusia', 'Fédération de Russie', 'Federazione Russa', 'Rusland'),
(646, 'RW', 'RWA', 'Rwanda', 'Ruanda', 'Rwanda', 'Ruanda', 'Rwanda', 'Ruanda', 'Rwanda'),
(654, 'SH', 'SHN', 'Svatá Helena', 'St. Helena', 'Saint Helena', 'Santa Helena', 'Sainte-Hélène', 'Sant''Elena', 'Sint-Helena'),
(659, 'KN', 'KNA', 'Svatý Kitts a Nevis', 'St. Kitts und Nevis', 'Saint Kitts and Nevis', 'Santa Kitts y Nevis', 'Saint-Kitts-et-Nevis', 'Saint Kitts e Nevis', 'Saint Kitts en Nevis'),
(660, 'AI', 'AIA', 'Anguilla', 'Anguilla', 'Anguilla', 'Anguilla', 'Anguilla', 'Anguilla', 'Anguilla'),
(662, 'LC', 'LCA', 'Svatá Lucie', 'St. Lucia', 'Saint Lucia', 'Santa Lucía', 'Sainte-Lucie', 'Santa Lucia', 'Saint Lucia'),
(666, 'PM', 'SPM', 'Svatý Pierre a Miquelon', 'St. Pierre und Miquelon', 'Saint-Pierre and Miquelon', 'San Pedro y Miquelon', 'Saint-Pierre-et-Miquelon', 'Saint Pierre e Miquelon', 'Saint-Pierre en Miquelon'),
(670, 'VC', 'VCT', 'Svatý Vincenc a Grenadiny', 'St. Vincent und die Grenadinen', 'Saint Vincent and the Grenadines', 'San Vincente y Las Granadinas', 'Saint-Vincent-et-les Grenadines', 'Saint Vincent e Grenadine', 'Saint Vincent en de Grenadines'),
(674, 'SM', 'SMR', 'San Marino', 'San Marino', 'San Marino', 'San Marino', 'Saint-Marin', 'San Marino', 'San Marino'),
(678, 'ST', 'STP', 'Svatý Tomáš a Princův ostrov', 'São Tomé und Príncipe', 'Sao Tome and Principe', 'Santo Tomé y Príncipe', 'Sao Tomé-et-Principe', 'Sao Tome e Principe', 'Sao Tomé en Principe'),
(682, 'SA', 'SAU', 'Saudská Arábie', 'Saudi-Arabien', 'Saudi Arabia', 'Arabia Saudí', 'Arabie Saoudite', 'Arabia Saudita', 'Saoedi-Arabië'),
(686, 'SN', 'SEN', 'Senegal', 'Senegal', 'Senegal', 'Senegal', 'Sénégal', 'Senegal', 'Senegal'),
(690, 'SC', 'SYC', 'Seychely', 'Seychellen', 'Seychelles', 'Seychelles', 'Seychelles', 'Seychelles', 'Seychellen'),
(694, 'SL', 'SLE', 'Sierra Leone', 'Sierra Leone', 'Sierra Leone', 'Sierra Leona', 'Sierra Leone', 'Sierra Leone', 'Sierra Leone'),
(702, 'SG', 'SGP', 'Singapur', 'Singapur', 'Singapore', 'Singapur', 'Singapour', 'Singapore', 'Singapore'),
(703, 'SK', 'SVK', 'Slovensko', 'Slowakei', 'Slovakia', 'Eslovaquia', 'Slovaquie', 'Slovacchia', 'Slowakije'),
(704, 'VN', 'VNM', 'Vietnam', 'Vietnam', 'Vietnam', 'Vietnam', 'Viet Nam', 'Vietnam', 'Vietnam'),
(705, 'SI', 'SVN', 'Slovinsko', 'Slowenien', 'Slovenia', 'Eslovenia', 'Slovénie', 'Slovenia', 'Slovenië'),
(706, 'SO', 'SOM', 'Somálsko', 'Somalia', 'Somalia', 'Somalia', 'Somalie', 'Somalia', 'Somalië'),
(710, 'ZA', 'ZAF', 'Jižní Afrika', 'Südafrika', 'South Africa', 'Sudáfrica', 'Afrique du Sud', 'Sud Africa', 'Zuid-Afrika'),
(716, 'ZW', 'ZWE', 'Zimbabwe', 'Simbabwe', 'Zimbabwe', 'Zimbabue', 'Zimbabwe', 'Zimbabwe', 'Zimbabwe'),
(724, 'ES', 'ESP', 'Španělsko', 'Spanien', 'Spain', 'España', 'Espagne', 'Spagna', 'Spanje'),
(732, 'EH', 'ESH', 'Západní Sahara', 'Westsahara', 'Western Sahara', 'Sáhara Occidental', 'Sahara Occidental', 'Sahara Occidentale', 'Westelijke Sahara'),
(736, 'SD', 'SDN', 'Súdán', 'Sudan', 'Sudan', 'Sudán', 'Soudan', 'Sudan', 'Sudan'),
(738, 'SS', 'SSD', 'Jižní Súdán', 'Südsudan', 'South Sudan', 'Sudán del Sur', 'Soudan du Sud', 'Sudan del Sud', 'Zuid-Soedan'),
(740, 'SR', 'SUR', 'Surinam', 'Suriname', 'Suriname', 'Surinám', 'Suriname', 'Suriname', 'Suriname'),
(744, 'SJ', 'SJM', 'Špicberky a Jan Mayen', 'Svalbard and Jan Mayen', 'Svalbard and Jan Mayen', 'Esvalbard y Jan Mayen', 'Svalbard etÎle Jan Mayen', 'Svalbard e Jan Mayen', 'Svalbard'),
(748, 'SZ', 'SWZ', 'Svazijsko', 'Swasiland', 'Swaziland', 'Suazilandia', 'Swaziland', 'Swaziland', 'Swaziland'),
(752, 'SE', 'SWE', 'Švédsko', 'Schweden', 'Sweden', 'Suecia', 'Suède', 'Svezia', 'Zweden'),
(756, 'CH', 'CHE', 'Švýcarsko', 'Schweiz', 'Switzerland', 'Suiza', 'Suisse', 'Svizzera', 'Zwitserland'),
(760, 'SY', 'SYR', 'Sýrie', 'Arabische Republik Syrien', 'Syrian Arab Republic', 'Siria', 'République Arabe Syrienne', 'Siria', 'Syrië'),
(762, 'TJ', 'TJK', 'Tadžikistán', 'Tadschikistan', 'Tajikistan', 'Tajikistán', 'Tadjikistan', 'Tagikistan', 'Tadzjikistan'),
(764, 'TH', 'THA', 'Thajsko', 'Thailand', 'Thailand', 'Tailandia', 'Thaïlande', 'Tailandia', 'Thailand'),
(768, 'TG', 'TGO', 'Togo', 'Togo', 'Togo', 'Togo', 'Togo', 'Togo', 'Togo'),
(772, 'TK', 'TKL', 'Tokelau', 'Tokelau', 'Tokelau', 'Tokelau', 'Tokelau', 'Tokelau', 'Tokelau -eilanden'),
(776, 'TO', 'TON', 'Tonga', 'Tonga', 'Tonga', 'Tongo', 'Tonga', 'Tonga', 'Tonga'),
(780, 'TT', 'TTO', 'Trinidad a Tobago', 'Trinidad und Tobago', 'Trinidad and Tobago', 'Trinidad y Tobago', 'Trinité-et-Tobago', 'Trinidad e Tobago', 'Trinidad en Tobago'),
(784, 'AE', 'ARE', 'Spojené Arabské Emiráty', 'Vereinigte Arabische Emirate', 'United Arab Emirates', 'EmiratosÁrabes Unidos', 'Émirats Arabes Unis', 'Emirati Arabi Uniti', 'Verenigde Arabische Emiraten'),
(788, 'TN', 'TUN', 'Tunisko', 'Tunesien', 'Tunisia', 'Túnez', 'Tunisie', 'Tunisia', 'Tunesië'),
(792, 'TR', 'TUR', 'Turecko', 'Türkei', 'Turkey', 'Turquía', 'Turquie', 'Turchia', 'Turkije'),
(795, 'TM', 'TKM', 'Turkmenistán', 'Turkmenistan', 'Turkmenistan', 'Turmenistán', 'Turkménistan', 'Turkmenistan', 'Turkmenistan'),
(796, 'TC', 'TCA', 'Turks a ostrovy Caicos', 'Turks- und Caicosinseln', 'Turks and Caicos Islands', 'Islas Turks y Caicos', 'Îles Turks et Caïques', 'Isole Turks e Caicos', 'Turks- en Caicoseilanden'),
(798, 'TV', 'TUV', 'Tuvalu', 'Tuvalu', 'Tuvalu', 'Tuvalu', 'Tuvalu', 'Tuvalu', 'Tuvalu'),
(800, 'UG', 'UGA', 'Uganda', 'Uganda', 'Uganda', 'Uganda', 'Ouganda', 'Uganda', 'Oeganda'),
(804, 'UA', 'UKR', 'Ukrajina', 'Ukraine', 'Ukraine', 'Ucrania', 'Ukraine', 'Ucraina', 'Oekraïne'),
(807, 'MK', 'MKD', 'Makedonie', 'Ehem. jugoslawische Republik Mazedonien', 'The Former Yugoslav Republic of Macedonia', 'Macedonia', 'L''ex-République Yougoslave de Macédoine', 'Macedonia', 'Macedonië'),
(818, 'EG', 'EGY', 'Egypt', 'Ägypten', 'Egypt', 'Egipto', 'Égypte', 'Egitto', 'Egypte'),
(826, 'GB', 'GBR', 'Velká Británie', 'Vereinigtes Königreich von Großbritannien und', 'United Kingdom', 'Reino Unido', 'Royaume-Uni', 'Regno Unito', 'Verenigd Koninkrijk'),
(833, 'IM', 'IMN', 'Ostrov Man', 'Insel Man', 'Isle of Man', 'Isla de Man', 'Île de Man', 'Isola di Man', 'Eiland Man'),
(834, 'TZ', 'TZA', 'Tanzánie', 'Vereinigte Republik Tansania', 'United Republic Of Tanzania', 'Tanzania', 'République-Unie de Tanzanie', 'Tanzania', 'Tanzania'),
(840, 'US', 'USA', 'USA', 'Vereinigte Staaten von Amerika', 'United States', 'Estados Unidos', 'États-Unis', 'Stati Uniti d''America', 'Verenigde Staten'),
(850, 'VI', 'VIR', 'Americké Panenské ostrovy', 'Amerikanische Jungferninseln', 'U.S. Virgin Islands', 'Islas Vírgenes Estadounidenses', 'Îles Vierges des États-Unis', 'Isole Vergini Americane', 'Amerikaanse Maagdeneilanden'),
(854, 'BF', 'BFA', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso'),
(858, 'UY', 'URY', 'Uruguay', 'Uruguay', 'Uruguay', 'Uruguay', 'Uruguay', 'Uruguay', 'Uruguay'),
(860, 'UZ', 'UZB', 'Uzbekistán', 'Usbekistan', 'Uzbekistan', 'Uzbekistán', 'Ouzbékistan', 'Uzbekistan', 'Oezbekistan'),
(862, 'VE', 'VEN', 'Venezuela', 'Venezuela', 'Venezuela', 'Venezuela', 'Venezuela', 'Venezuela', 'Venezuela'),
(876, 'WF', 'WLF', 'Wallis a Futuna', 'Wallis und Futuna', 'Wallis and Futuna', 'Wallis y Futuna', 'Wallis et Futuna', 'Wallis e Futuna', 'Wallis en Futuna'),
(882, 'WS', 'WSM', 'Samoa', 'Samoa', 'Samoa', 'Samoa', 'Samoa', 'Samoa', 'Samoa'),
(887, 'YE', 'YEM', 'Jemen', 'Jemen', 'Yemen', 'Yemen', 'Yémen', 'Yemen', 'Jemen'),
(891, 'CS', 'SCG', 'Serbia and Montenegro', 'Serbien und Montenegro', 'Serbia and Montenegro', 'Serbia y Montenegro', 'Serbie-et-Monténégro', 'Serbia e Montenegro', 'Servië en Montenegro'),
(894, 'ZM', 'ZMB', 'Zambie', 'Sambia', 'Zambia', 'Zambia', 'Zambie', 'Zambia', 'Zambia');

INSERT INTO Users (username, name, middleName, lastName, image, email, password, status, type, createdAt, lastConnection, birthday, countryCode) VALUES
('admin', 'Administrador', '-', 'Administrador', 'default.png', 'iptv-admin@gmail.com','5e5331c1bbb18aa1da25451debea6552',1,2,current_timestamp,'2017-05-08 14:38:45.669945','1988-07-26',152);

INSERT INTO Channel (idChannel, number, name, logo, language, countryCode, description, score) VALUES
(1, 3, 'NAT GEO KIDS', 'natgeo_kids.jpg', 'esp', 0, 'Enfocado a niños de 4 a 7 años con programación preescolar de temática dirigida a la ciencia, medio ambiente y a la exploración', 0),
(2, 4, 'TOONCAST', 'tooncast.jpg', 'esp', 0, 'Solo toons, todo el tiempo.', 0),
(3, 6, 'BABY TV', 'baby_tv.jpg', 'esp', 0, 'Canal creado principalmente para bebés.', 0),
(4, 7, 'NICK JR.', 'nick_jr.jpg', 'esp', 0, 'Canal para los niños más pequeños del hogar.', 0),
(5, 8, 'DISNEY JUNIOR', 'disney_jr.jpg', 'esp', 0, 'Programación para niños en edad pre-escolar.', 0),
(6, 9, 'DISNEY CHANNEL', 'disney_chnl.jpg', 'esp', 0, 'Canal dedicado a los niños y la familia.', 0),
(7, 10, 'DISCOVERY KIDS', 'discovery_kids.jpg', 'esp', 0, 'Programación de alta calidad para los más pequeños.', 0),
(8, 11, 'NICKELODEON', 'nickelodeon.jpg', 'esp', 0, 'Canal exclusivo para niños.', 0),
(9, 12, 'DISNEY XD', 'disney_xd.jpg', 'esp', 0, 'Canal para los niños más pequeños del hogar.', 0),
(10, 13, 'BOOMERANG', 'boomerang.jpg', 'esp', 0, 'Transmite las mejores series, películas,musicales y animaciones para niños.', 0),
(11, 14, 'CARTOON NETWORK', 'cartoon_network.jpg', 'esp', 0, 'Programación infantil para niños de 4 a 8 años.', 0),
(12, 15, 'ETC TV', 'default.png', 'esp', 0, 'Para los fanáticos del anime.', 0),
(13, 17, 'LA RED', 'la_red.jpg', 'esp', 152, 'Lo mejor de los canales nacionales.', 0),
(14, 18, 'UCV', 'ucv.png', 'esp', 152, 'Lo mejor de los canales nacionales.', 0),
(15, 19, 'TVN', 'tvn.jpg', 'esp', 152, 'Cuenta con una amplia gama de programación, la cual se rige por su línea editorial.', 0),
(16, 20, 'MEGA', 'mega_cl.jpg', 'esp', 152, 'Se vive.', 0),
(17, 21, 'CHILEVISIÓN', 'chv.jpg', 'esp', 152, 'Te ve de verdad.', 0),
(18, 22, 'CANAL 13', '13.jpg', 'esp', 152, 'Lo mejor de los canales nacionales.', 0),
(19, 27, 'CANAL TV MAS', 'tv_mas.jpg', 'esp', 152, 'Canal de televisión temático de más amplia cobertura en el país mediante una producción de calidad.', 0),
(20, 28, 'CANAL DE LOS RECUERDOS', 'cr_tv.png', 'esp', 152, 'Canal Chileno', 0),
(21, 29, 'MEGA NOTICIAS', 'an.png', 'esp', 152, 'Canal Chileno', 0),
(22, 30, 'T13 MÓVIL', 't13_m.jpg', 'esp', 152, 'Canal Chileno', 0),
(23, 31, 'ADN', 'adn.jpg', 'esp', 152, 'Canal Chileno', 0),
(24, 32, 'CNN CHILE', 'cnn_cl.png', 'esp', 152, 'Canal Chileno', 0),
(25, 33, 'CARNAVAL', 'carnaval.png', 'esp', 152, 'Canal Chileno', 0),
(26, 34, 'TV SENADO', 'tv_senado_cl.jpeg', 'esp', 152, 'Canal Chileno', 0),
(27, 35, 'CÁMARA DE DIPUTADOS', 'diputados_cl.png', 'esp', 152, 'Canal Chileno', 0),
(28, 36, 'UES TV', 'uestv_cl.png', 'esp', 152, 'Canal Chileno', 0),
(29, 37, 'UNIVERSIDAD AUTONOMA', 'uatv_cl.png', 'esp', 152, 'Canal Chileno', 0),
(30, 38, 'MORROVISION', 'morrovision.jpg', 'esp', 152, 'Canal Chileno', 0),
(31, 39, 'UCV GRAN VALPARAÍSO', 'ucv_valp.png', 'esp', 152, 'Canal Chileno', 0),
(32, 40, 'CANAL 2 DE SAN ANTONIO', 'c2_sn_ant.jpg', 'esp', 152, 'Canal Chileno', 0),
(33, 41, 'TV MAULE', 'tvmaule.png', 'esp', 152, 'Canal Chileno', 0),
(34, 42, 'CANAL 9 BÍO BÍO', 'c9_biobio.png', 'esp', 152, 'Canal Chileno', 0),
(35, 43, 'ÑUBLE TV', 'nubletv.jpg', 'esp', 152, 'Canal Chileno', 0),
(36, 44, 'CANAL 33 TEMUCO', 'c33_temuco.jpg', 'esp', 152, 'Canal Chileno', 0),
(37, 45, 'CANAL 2 QUELLÓN', 'c2_quellon.png', 'esp', 152, 'Canal Chileno', 0),
(38, 46, 'PINGUINO TV', 'pinguino_tv.png', 'esp', 152, 'Canal Chileno', 0),
(39, 47, 'TVRED PUNTA ARENAS', 'tv_red_pa.jpg', 'esp', 152, 'Canal Chileno', 0),
(40, 48, 'NIU TV', 'niu_tv.png', 'esp', 152, 'Canal Chileno', 0),
(41, 49, 'ARICA TV', 'arica_tv.jpg', 'esp', 152, 'Canal Chileno', 0),
(42, 50, 'CANAL 38 CARNAVAL', 'carnaval.png', 'esp', 152, 'Canal Chileno', 0),
(43, 51, 'SEXTA VISIÓN', 'sexta_vision.png', 'esp', 152, 'Canal Chileno', 0),
(44, 52, 'INET TV', 'inet_tv.png', 'esp', 152, 'Canal Chileno', 0),
(45, 53, 'ANTOFAGASTA TV', 'antofagasta_tv.png', 'esp', 152, 'Canal Chileno', 0),
(46, 54, 'ChiloéTV', 'chiloe_tv.png', 'esp', 152, 'Canal Chileno', 0),
(47, 55, 'TVO SAN VICENTE', 'tvo.png', 'esp', 152, 'Canal Chileno', 0),
(48, 56, 'CONTIVISIÓN', 'contivision.png', 'esp', 152, 'Canal Chileno', 0),
(49, 57, 'TV CENTRO', 'tvcentro.jpg', 'esp', 152, 'Canal Chileno', 0),
(50, 58, 'CANAL 5 LINARES', 'tvlinares.jpg', 'esp', 152, 'Canal Chileno', 0),
(51, 59, 'CANAL 21 CHILLÁN', 'c21_chillan.jpg', 'esp', 152, 'Canal Chileno', 0),
(52, 60, 'ITV PATAGONIA', 'itv_patagonia.png', 'esp', 152, 'Canal Chileno', 0),
(53, 61, 'VTV ACONCAGUA', 'vtv_aconcagua.jpg', 'esp', 152, 'Canal Chileno', 0),
(54, 62, 'VTV VIÑA DEL MAR', 'vtv_vina_mar.jpg', 'esp', 152, 'Canal Chileno', 0),
(55, 63, 'THEMATV', 'thematv.png', 'esp', 152, 'Canal Chileno', 0),
(56, 64, 'TELEVIDA HD', 'televida_hd.png', 'esp', 152, 'Canal Chileno', 0),
(57, 65, 'GRACIA TV', 'gracia_tv.png', 'esp', 152, 'Canal Chileno', 0),
(58, 66, 'TELETRACK', 'teletrack.png', 'esp', 152, 'Canal Chileno', 0),
(59, 67, 'FM HOT TV HD', 'fm_hot_tv_hd.jpg', 'esp', 152, 'Canal Chileno', 0),
(60, 68, 'CAROLINA TV', 'carolina_tv.png', 'esp', 152, 'Canal Chileno', 0),
(61, 69, 'CONTIVISIÓN MUSICA', 'contivision_musica.png', 'esp', 152, 'Canal Chileno', 0),
(62, 70, 'CDO', 'cdo_cl.jpg', 'esp', 152, 'Canal Chileno', 0),
(63, 151, 'TBS', 'tbs.jpg', 'esp', 0, 'Canal dedicado a transmitir programas de humor para toda la familia.', 0),
(64, 152, 'WARNER CHANNEL', 'warner.jpg', 'esp', 0, 'Canal de entretención donde se exhiben las mejores series del momento.', 0),
(65, 153, 'AXN', 'axn.jpg', 'esp', 0, 'Canal de series de acción y deporte aventura.', 0),
(66, 154, 'SONY ENTERTAINMENT', 'sony_ent.jpg', 'esp', 0, 'Ofrece las series más premiadas y éxitosas del último tiempo.', 0),
(67, 155, 'FOX', 'fox.jpg', 'esp', 0, 'Canal de series más vistos de América Latina, haciendo gala de una mezcla perfecta entre series y cine.', 0),
(68, 156, 'A&E MUNDO', 'ae_mundo.jpg', 'esp', 0, 'A&E se destaca por su contenido de puro entretenimiento: nuevas películas, extraordinarios conciertos y festivales musicales, nuevas series y mini series de las mejores productoras del mundo.', 0),
(69, 157, 'UNIVERSAL CHANNEL', 'universal.jpg', 'esp', 0, 'Orientado a la entretención con un marcado énfasis en el género del crimen y la investigación.', 0),
(70, 158, 'FOXLIFE', 'foxlife.png', 'esp', 0, 'Fox Life centra su atención en la mujer actual, con series y películas cuidadosamente seleccionadas para ellas.', 0),
(71, 159, 'E! ENTERTAINMENT', 'entertaiment.jpg', 'esp', 0, 'Canal del mundo del espectáculo y la entretención.', 0),
(72, 160, 'INVESTIGATION DISCOVERY', 'investigation_discovery.jpg', 'esp', 0, 'El primer y único canal de entretenimiento enfocado en el suspenso, crimen e investigación.', 0),
(73, 161, 'FX', 'fx.jpg', 'esp', 0, 'Canal diseñado para hombres que quieren disfrutar de una programación especialmente para ellos.', 0),
(74, 162, 'AZTECA CORAZÓN', 'azteca_corazon.jpg', 'esp', 0, 'Transmitirá los 365 días al año y las 24 horas del día, las espectaculares telenovelas que han conmovido a millones de televidentes .', 0),
(75, 163, 'GLITZ*', 'glitz.jpg', 'esp', 0, 'Para los amantes de la moda.', 0),
(76, 165, 'SYFY', 'syfy.jpg', 'esp', 0, 'Pura ciencia ficción las 24 hrs. del día.', 0),
(77, 166, 'TRUTV', 'trutv.jpg', 'esp', 0, 'Canal para televidentes a los que les gusta vivenciar emociones y estar cerca de la acción, casi de forma personal.', 0),
(78, 167, 'TNT SERIES', 'tnt_series.jpg', 'esp', 0, 'Canal dedicado exclusivamente a las series de televisión contemporáneas.', 0),
(79, 168, 'COMEDY CENTRAL', 'comedy_central.jpg', 'esp', 0, 'Canal que transmite las mejores series cómicas norteamericanas.', 0),
(80, 169, 'LIFETIME', 'lifetime.jpg', 'esp', 0, 'Canal de entretenimiento variado que ofrece películas y series dramáticas, series no ficción junto con sus ya reconocidas y galardonadas películas originales de Lifetime Movies.', 0),
(81, 174, 'ESPN', 'espn.jpg', 'esp', 0, 'Líder mundial en deportes, el cual entrega información completa de todo tipo de deportes.', 0),
(82, 175, 'ESPN2', 'espn2.jpg', 'esp', 0, 'Señal derivada de ESPN, que ofrece contenido propio latinoamericano.', 0),
(83, 176, 'ESPN3', 'espn3.jpg', 'esp', 0, 'Señal dedicada a un público más joven, su perfil es de hombres entre 16 y 24 años de edad', 0),
(84, 200, 'Antena 3 Internacional', 'A3Internacional.png', 'esp', 0, '-', 0),
(85, 300, 'Anime TV', 'default.png', 'esp', 0, '-', 0),
(86, 201, 'Canal 4', 'default.png', 'esp', 0, '-', 0),
(87, 300, 'MTV', 'MTV.png', 'esp', 0, 'Canal de Musica', 0),
(88, 202, 'HBO', 'hbo.png', 'esp', 0, 'HBO HD', 0),
(89, 1000, 'Los Simpson (Random)', 'simpsons.png', 'esp', 0, 'HBO HD', 0),
(90, 1001, 'Futurama (Random)', 'futurama.png', 'esp', 0, 'HBO HD', 0),
(91, 500, 'CDF Basico', 'cdf.png', 'esp', 0, 'Canal de Deportes', 0),
(92, 501, 'CDF HD', 'cdf_hd.png', 'esp', 0, 'Canal de Deportes', 0),
(93, 502, 'CDF Full HD', 'default.png', 'esp', 0, 'Canal de Deportes', 0),
(94, 503, 'CDF Premium', 'cdf_p.png', 'esp', 0, 'Canal de Deportes', 0),
(95, 601, 'History Channel', 'history.jpg', 'esp', 0, 'Canal de Deportes', 0),
(96, 602, 'History HD', 'history_hd.png', 'esp', 0, 'History Channel', 0),
(97, 603, 'Discovery H&H', 'discovery_hh.png', 'esp', 0, 'Discovery', 0),
(98, 604, 'Discovery H&H HD', 'discovery_hh_hd.png', 'esp', 0, 'Discovery', 0),
(99, 177, 'FOX Series', 'Fox-series.png', 'esp', 0, 'Fox Series', 0),
(100, 605, 'NatGeo Wild', 'Nat_Geo_Wild.svg', 'esp', 0, 'NatGeo Wild', 0),
(101, 178, 'TCL', 'tlc.png', 'esp', 0, 'TLC', 0),
(102, 202, 'Canal 5 Televisa', 'televisa5.png', 'esp', 0, 'TLC', 0);

INSERT INTO Link (url, idChannel, ranking, status) VALUES
('http://d28d7tc1ojykcy.cloudfront.net/lared/smil:lared.smil/playlist.m3u8',13, 100, 1),
('http://unlimited5-cl.digitalproserver.com/ucvtv/ucvtv.smil/playlist.m3u8',14, 100, 1),
('http://mdstrm.com/live-stream-playlist/57a498c4d7b86d600e5461cb.m3u8',15, 100, 1),
('http://mdstrm.com/live-stream-playlist_1500/53d2c1a32640614e62a0e000.m3u8',16, 100, 1),
('http://live.hls.http.chv.ztreaming.com/chvhddesktop/chv-desktop.m3u8',17, 100, 1),
('http://13.live.cdn.cl/13hddesktop/13hd-1250.m3u8',18, 100, 1),
('http://wow2.cl.digitalproserver.com/recuerdos/recuerdosvivo/recuerdos.smil/playlist.m3u8',20, 100, 1),
('http://mdstrm.com/live-stream-playlist/561430ae330428c223687e1e.m3u8',21, 100, 1),
('http://cdn.ztreaming.com/t13vivo3/mobile.m3u8',22, 100, 1),
('http://unlimited5-cl.dps.live/adntv/adntv.smil/playlist.m3u8',23, 100, 1),
('http://unlimited7-cl.dps.live/cnn/cnn.smil/playlist.m3u8',24, 100, 1),
('http://wow2.cl.digitalproserver.com/carnavaltv/carnavaltvvivo/carnavaltv.smil/chunklist.m3u8',25, 100, 1),
('http://janus-tv-ply.senado.cl/playlist/stream.m3u8?s=tvsenado-hd',26, 100, 1),
('http://camara.03.cl.cdnz.cl/camara19/live/chunklist.m3u8',27, 100, 1),
('http://cl.origin.grupoz.cl/uestv/live/playlist.m3u8',28, 100, 1),
('http://v1.tustreaming.cl:1935/tvautonoma/tvautonoma/playlist.m3u8',29, 100, 1),
('http://video.crearchile.com:8080/live/mp4:morrovision.mp4/playlist.m3u8',30, 100, 1),
('http://wow2.cl.digitalproserver.com/ucvtv2/ucvtv2vivo/ucvtv2.smil/chunklist_w870498386_b1428000.m3u8',31, 100, 1),
('http://video.crearchile.com:8080/live/canal2.mp4/playlist.m3u8',32, 100, 1),
('http://sietransdemos.com/iptv/tvmaule.m3u8',33, 100, 1),
('http://unlimited6-cl.dps.live/c9/c9.smil/c9/livestream1/chunks.m3u8',34, 100, 1),
('https://593b04c4c5670.streamlock.net:443/tvdaniel/tvdaniel/playlist.m3u8',35, 100, 1),
('http://190.107.176.55:1935/live/canal-33/playlist.m3u8',36, 100, 1),
('http://wow2.cl.digitalproserver.com/tvquellon/tvquellonvivo/tvquellon.smil/playlist.m3u8',37, 100, 1),
('http://streaming.elpinguino.com:1935/live/pinguinotv_240p/playlist.m3u8',38, 100, 1),
('http://69.164.193.177:1935/live/myStream/master.m3u8',39, 100, 1),
('http://cl.origin.grupoz.cl/niutv/smil:live.smil/playlist.m3u8',40, 100, 1),
('http://wow2.cl.digitalproserver.com/aricatv/aricatvvivo/aricatv.smil/chunklist_w1256972144_b848000.m3u8',41, 100, 1),
('http://wow2.cl.digitalproserver.com/carnavaltv/carnavaltvvivo/mp4:livestream2/playlist.m3u8',42, 100, 1),
('http://cl.streaminghd.cl/carlos132/carlos132/chunklist_w565655987.m3u8',43, 100, 1),
('http://wow2.cl.digitalproserver.com/inet2/inet2vivo/inet2.smil/chunklist_w1511777064_b828000.m3u8',44, 100, 1),
('http://wow2.cl.digitalproserver.com/atv/atvvivo/atv.smil/chunklist_w1499498801_b1428000.m3u8',45, 100, 1),
('http://streaminglivehd.com:1935/8038/8038/playlist.m3u8',46, 100, 1),
('http://movilstream.digitalproserver.com/tvo/tvovivo/tvo.smil/playlist.m3u8',47, 100, 1),
('http://movilstream.digitalproserver.com/contivisionhd/contivisionhdvivo/contivisionhd.smil/playlist.m3u8',48, 100, 1),
('http://wow2.cl.digitalproserver.com/centrotv/centrotvvivo/centrotv.smil/playlist.m3u8',49, 100, 1),
('http://v3.tustreaming.cl/tv5/live1/tracks-v1a1/event-999999.m3u8',50, 100, 1),
('http://v3.tustreaming.cl/canal21chillan/live1/tracks-v1a1/event-999999.m3u8',51, 100, 1),
('http://movilstream.digitalproserver.com/itv/itvvivo/mp4:livestream1/playlist.m3u8',52, 100, 1),
('http://cdn.streamingmedia.cl:1935/live/vtv2/livestream3/playlist.m3u8',53, 100, 1),
('http://cdn.streamingmedia.cl:1935/live/vtvvina/playlist.m3u8',54, 100, 1),
('http://wow2.cl.digitalproserver.com/thema/themavivo/thema.smil/chunklist_w1551564427_b848000.m3u8',55, 100, 1),
('http://unlimited5-cl.dps.live/tlvida/tlvida.smil/tlvida/livestream1/chunks.m3u8',56, 100, 1),
('http://v3.tustreaming.cl:80/graciatv/live1/index.m3u8',57, 100, 1),
('http://edge1.cl.grupoz.cl/teletrak//live/playlist.m3u8',58, 100, 1),
('http://evo.eltelon.com:1935/live/fm-hot-tv/playlist.m3u8',59, 100, 1),
('http://unlimited6-cl.dps.live/carolinatv/carolinatv.smil/carolinatv/livestream2/chunks.m3u8',60, 100, 1),
('http://movilstream.digitalproserver.com/contivisionmusica/contivisionmusicavivo/contivisionmusica.smil/chunklist_w205005636_b1428000.m3u8',61, 100, 1),
('http://edge1.cl.grupoz.cl/cdofree/live/chunklist_w102518198.m3u8',62, 100, 1),
('http://a3live-lh.akamaihd.net:80/i/antena3_1@35248/master.m3u8',84, 100, 1),
('http://video7.stream.mx:1935/raxatv/raxatv/chunklist_w884577146.m3u8',85,100,1),
('http://streaming.wirenetserver.com.ar:1935/canal4/live_1/playlist.m3u8',86,90,1),
('http://3-120heat.pulsedmedia.com/public-llave1/list/public/cart_net.php/index.m3u8',11,90,1),
('http://3-120heat.pulsedmedia.com/public-llave1/list/public/natgeo_kids.php/index.m3u8',1,90,1),
('http://3-120heat.pulsedmedia.com/public-llave1/list/public/disc_kids.php/index.m3u8',7,90,1),
('http://3-120heat.pulsedmedia.com/public-llave1/list/public/d1sney.php/index.m3u8',6,90,1),
('http://3-120heat.pulsedmedia.com/public-llave1/list/public/d1sneyjr.php/index.m3u8',5,90,1),
('http://3-120heat.pulsedmedia.com/public-llave1/list/public/d1sneyxd.php/index.m3u8',9,90,1),
('http://3-120heat.pulsedmedia.com/public-llave1/list/public/nick.php/index.m3u8',4,90,1),
('http://3-120heat.pulsedmedia.com/public-llave1/list/public/tooncast.php/index.m3u8',2,90,1),
('http://3-120heat.pulsedmedia.com/public-llave1/list/public/tnt_ser.php/index.m3u8',78,90,1),
('http://3-120heat.pulsedmedia.com/public-llave1/list/public/f0x.php/index.m3u8',67,90,1),
('http://3-120heat.pulsedmedia.com/public-llave1/list/public/fx.php/index.m3u8',73,90,1),
('http://3-120heat.pulsedmedia.com/public-llave1/list/public/hb0hd.php/index.m3u8',88,90,1),
('http://3-120heat.pulsedmedia.com/public-llave1/list/public/aehd.php/index.m3u8',68,90,1),
('http://3-120heat.pulsedmedia.com/public-llave1/list/public/warner.php/index.m3u8',64,90,1),
('http://tecnotv.xyz/simpson1.php',89,90,1),
('http://tecnotv.xyz/futurama1.php',90,90,1),
('http://tecnotv.xyz/tvmex/mtvchannel',87,90,1),
('http://ragnaroktv.com/hls/kr/ESPN_3_HD/M7KwNDA0M7MwNQAA/espn3_kr.m3u8',83,90,1),
('http://s1.iptv61w.com:25461/live/mary/54321/3679.ts',91,90,1),
('http://s1.iptv61w.com:25461/live/mary/54321/8228.ts',92,90,1),
('http://s1.iptv61w.com:25461/live/mary/54321/3934.ts',93,90,1),
('http://s1.iptv61w.com:25461/live/mary/54321/3680.ts',94,90,1),
('http://s1.iptv61w.com:25461/live/mary/54321/3747.ts',65,90,1),
('http://s1.iptv61w.com:25461/live/mary/54321/3792.ts',95,90,1),
('http://s1.iptv61w.com:25461/live/mary/54321/7472.ts',96,90,1),
('http://s1.iptv61w.com:25461/live/mary/54321/3765.ts',97,90,1),
('http://s1.iptv61w.com:25461/live/mary/54321/7470.ts',98,90,1),
('http://s1.iptv61w.com:25461/live/mary/54321/3796.ts',72,90,1),
('http://98.109.150.239:8001/play/a01r/index.m3u8',99,90,1),
('http://98.109.150.239:8001/play/a01n/index.m3u8',100,90,1),
('http://98.109.150.239:8001/play/a01f/index.m3u8',101,90,1),
('http://98.109.150.239:8001/play/a01p/index.m3u8', 102, 90, 1);

INSERT INTO parCategoryChannel (idcategory, idchannel) VALUES
(1,1),
(1,2),
(1,3),
(1,4),
(1,5),
(1,6),
(1,7),
(1,8),
(1,9),
(1,10),
(1,11),
(1,12),
(2,13),
(2,14),
(2,15),
(2,16),
(2,17),
(2,18),
(2,19),
(2,20),
(2,31),
(2,32),
(2,33),
(2,34),
(2,35),
(2,36),
(2,37),
(2,38),
(2,39),
(2,40),
(2,41),
(2,42),
(2,43),
(2,44),
(2,45),
(2,46),
(2,47),
(2,48),
(2,49),
(2,50),
(2,51),
(2,52),
(2,53),
(2,54),
(2,55),
(2,56),
(2,57),
(2,58),
(2,59),
(2,60),
(2,61),
(2,62),
(10,63),
(10,64),
(10,65),
(10,66),
(10,67),
(10,68),
(10,69),
(10,70),
(10,71),
(10,72),
(10,73),
(0,74),
(10,75),
(10,76),
(10,77),
(10,78),
(10,79),
(10,80),
(7,81),
(7,82),
(7,83),
(2, 84),
(10, 85),
(2, 86),
(6, 87),
(15, 88),
(10, 89),
(10, 90),
(7, 91),
(7, 92),
(7, 93),
(7, 94),
(12, 95),
(12, 96),
(12, 97),
(12, 98),
(10,99),
(12, 100),
(10, 101),
(10, 102);
