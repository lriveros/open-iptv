package models

import (
	"fmt"
	"time"
)

type User struct {
	ID             int
	Username       string
	Name           string
	MiddleName     string //posiblemente nulo
	LastName       string
	Image          string //posiblemente nulo
	Email          string
	Password       string
	Status         int
	Type           int
	CreatedAt      time.Time
	LastConnection time.Time
	Birthay        time.Time
	CountryCode    int
}

func NewUser() User {
	var user User
	return user
}

/**Login de suaurio**/
func Login(username string, password string) (User, error) {
	query := fmt.Sprintf(queryLogin, username, username, password)
	//log.Print(query)
	rows, err := db.Query(query)
	var user User
	//log.Print("Consultando a base de datos")
	if err != nil {
		return user, err
	}
	defer rows.Close()
	//usuario encontrado
	for rows.Next() {
		var usr User
		//id, nombres, apellidos, imagen, email, username, status, tipo, created_at, last_connection
		err := rows.Scan(&usr.ID, &usr.Username, &usr.Name, &usr.MiddleName, &usr.LastName, &usr.Image, &usr.Email, &usr.Status, &usr.Type, &usr.CreatedAt, &usr.LastConnection, &usr.Birthay, &usr.CountryCode)
		if err != nil {
			return user, err
		}
		user = usr
	}
	if err = rows.Err(); err != nil {
		return user, err
	}
	//outgoingJSON, _ := json.Marshal(user)
	//log.Print(fmt.Sprintf("Retornando al usuario %s", string(outgoingJSON)))
	updateLastConnection(user.ID)
	return user, nil
}

func updateLastConnection(idUser int) {
	query := fmt.Sprintf(uUserLoginTime, idUser)
	//log.Print(query)
	db.Exec(query)
}

func InsertUser(u User) (bool, error) {
	query := fmt.Sprintf(iUser, u.Username, u.Name, u.LastName, u.Email, u.Password)
	//log.Print(query)

	row, err := db.Exec(query)

	if err != nil {
		return false, err
	}

	_, err = row.LastInsertId()

	if err != nil {
		return false, err
	}

	return true, nil
}

/*
func AllUsers() ([]*User, error) {
	rows, err := db.Query("SELECT * FROM user")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := make([]*User, 0)
	for rows.Next() {
		usr := new(Book)
		err := rows.Scan(&usr.Isbn, &usr.Title, &usr.Author, &usr.Price)
		if err != nil {
			return nil, err
		}
		usr = append(users, usr)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return users, nil
}
*/
