package models

import (
	"fmt"
	"log"
)

type Channel struct {
	ID          int
	Number      int
	Name        string
	Logo        string
	Language    string
	Country     int
	Description string
	Score       float32
	Links       []Link
}

type ChannelList struct {
	Number int
	Name   string
	Logo   string
	Url    string
	Groups string
}

func NewChannel() *Channel {
	return &Channel{}
}

func NewChannelList() *ChannelList {
	return &ChannelList{}
}

func GetChannel(idChannel int) (Channel, error) {
	query := fmt.Sprintf(qChannel, idChannel)
	var chnl Channel
	//log.Print(query)
	rows, err := db.Query(query)

	if err != nil {
		return chnl, err
	}
	defer rows.Close()

	//log.Print(rows)
	if rows.Next() {
		//idChannel
		err := rows.Scan(&chnl.ID, &chnl.Number, &chnl.Name, &chnl.Logo, &chnl.Language, &chnl.Country, &chnl.Description, &chnl.Score)
		//log.Print("Canal Encontrado", chnl)
		if err != nil {
			return chnl, err
		}
	}
	chnl.Links, err = GetLinks(idChannel)
	return chnl, err
}

func GetChannelList(wd string) ([]Channel, error) {
	var query string
	if wd == "" {
		query = fmt.Sprintf(qTopChannels, "10")
	} else {
		query = fmt.Sprintf(qChannelsSearch, wd)
	}
	//log.Print(query)
	rows, err := db.Query(query)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	channels := make([]Channel, 0)
	//log.Print(rows)
	for rows.Next() {
		var chnl Channel
		//idChannel
		err := rows.Scan(&chnl.ID, &chnl.Number, &chnl.Name, &chnl.Logo, &chnl.Language, &chnl.Country, &chnl.Description, &chnl.Score)
		//log.Print("Canal Encontrado", chnl)
		if err != nil {
			return nil, err
		}
		channels = Extend(channels, chnl)
		//channels = append(channels, chnl)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	//outgoingJSON, _ := json.Marshal(channels)
	//log.Print(fmt.Sprintf("Retornando Listado de Canales %s", string(outgoingJSON)))
	return channels, nil
}

func GetChannelMainList() ([]ChannelList, error) {
	rows, err := db.Query(qChnlLst)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	channels := make([]ChannelList, 0)

	for rows.Next() {
		var chnl ChannelList
		//idChannel
		err := rows.Scan(&chnl.Number, &chnl.Name, &chnl.Logo, &chnl.Url, &chnl.Groups)
		if err != nil {
			return nil, err
		}
		channels = ExtendCL(channels, chnl)
		//channels = append(channels, chnl)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	//outgoingJSON, _ := json.Marshal(channels)
	//log.Print(fmt.Sprintf("Retornando Listado de Canales %s", string(outgoingJSON)))
	return channels, nil
}

//Insertar Canal
func InsertChannel(chnl Channel, cat int) (bool, error) {
	query := fmt.Sprintf(iChannel, chnl.Name, chnl.Logo, chnl.Description)
	log.Print(query)
	_, err := db.Exec(query)
	if err != nil {
		return false, err
	}

	query2 := fmt.Sprintf(qLastIDChannel, chnl.Name)
	row, err := db.Query(query2)

	if err != nil {
		return false, err
	}
	defer row.Close()

	var ch Channel
	//log.Print(rows)
	if row.Next() {
		err = row.Scan(&ch.ID)
		//log.Print("Canal Encontrado", chnl)
		if err != nil {
			return false, err
		}
	}

	if ch.ID == 0 {
		return false, nil
	}
	query3 := fmt.Sprintf(iCatergoryChannel, cat, ch.ID)

	_, err = db.Exec(query3)

	if err != nil {
		return false, err
	}

	return true, nil
}

func InsertChannelAction(idChannel int, idUser int, idAction int) (bool, error) {
	query := fmt.Sprintf(iAcctionChannel, idChannel, idUser, idAction)
	//log.Print(query)

	_, err := db.Exec(query)

	if err != nil {
		return false, err
	}

	return true, nil
}

func ExtendCL(slice []ChannelList, element ChannelList) []ChannelList {
	n := len(slice)
	if n == cap(slice) {
		// Slice is full; must grow.
		// We double its size and add 1, so if the size is zero we still grow.
		newSlice := make([]ChannelList, len(slice), 2*len(slice)+1)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0 : n+1]
	slice[n] = element
	return slice
}

func Extend(slice []Channel, element Channel) []Channel {
	n := len(slice)
	if n == cap(slice) {
		// Slice is full; must grow.
		// We double its size and add 1, so if the size is zero we still grow.
		newSlice := make([]Channel, len(slice), 2*len(slice)+1)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0 : n+1]
	slice[n] = element
	return slice
}
