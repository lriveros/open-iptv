package models

import (
	"fmt"
	"log"
)

type Link struct {
	ID        int
	Url       string
	IdChannel int
	Ranking   float32
	Status    string
}

func NewLink() *Link {
	return &Link{}
}

func GetLinks(idChannel int) ([]Link, error) {
	var query string
	if idChannel == -1 {
		query = fmt.Sprintf(qActiveLink)
	} else {
		query = fmt.Sprintf(qChannelLinks, idChannel)
	}
	//log.Print(query)
	rows, err := db.Query(query)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	links := make([]Link, 0)
	//log.Print(rows)
	for rows.Next() {
		var link Link
		//idChannel
		err := rows.Scan(&link.ID, &link.IdChannel, &link.Url, &link.Status, &link.Ranking)
		//log.Print("Link Encontrado", link)
		if err != nil {
			return nil, err
		}
		links = ExtendLink(links, link)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return links, nil
}

func InsertLink(idChannel int, link string, idUser string) (bool, error) {
	query := fmt.Sprintf(iChnlLink, link, idChannel)
	//log.Print(query)
	row, err := db.Exec(query)

	if err != nil {
		return false, err
	}

	r, err := row.LastInsertId()
	log.Print(r)

	if err != nil {
		return false, err
	}

	return r > 0, nil
}

func UpdateLink(idLink int, status int) (bool, error) {
	query := fmt.Sprintf(uLink, status, idLink)
	//log.Print(query)
	_, err := db.Exec(query)

	if err != nil {
		return false, err
	}

	return true, nil
}

func InsertLinkAction(idLink int, idUser int, idAction int) (bool, error) {
	query := fmt.Sprintf(iAccionLink, idLink, idUser, idAction)
	//log.Print(query)

	_, err := db.Exec(query)

	if err != nil {
		return false, err
	}

	return true, nil
}

func ExtendLink(slice []Link, element Link) []Link {
	n := len(slice)
	if n == cap(slice) {
		// Slice is full; must grow.
		// We double its size and add 1, so if the size is zero we still grow.
		newSlice := make([]Link, len(slice), 2*len(slice)+1)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0 : n+1]
	slice[n] = element
	return slice
}
