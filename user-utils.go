package main

import (
	"crypto/md5"
	"fmt"
	"io"
)

func encriptarPassword(pass string) string {
	h := md5.New()
	io.WriteString(h, pass)
	password := fmt.Sprintf("%x", h.Sum(nil))
	return password
}
