package main

const (
	urlMainList       = "/mainList.m3u"
	mainListFile      = "lists/main.m3u"
	nameCountGetList  = "countGetlist"
	nameCountUsere    = "countUsers"
	insertLinkCode    = 1
	voteUpLinkCode    = 2
	voteDownLinkCode  = 3
	voteInactiveCode  = 4
	statusOk          = "success"
	statusError       = "error"
	msgUsuarioRegNoOk = "Usuario No se ha podido ingresar, valide los campos e intente nuevamente"
	msgUsuarioRegOk   = "Usuario Ingresado correctamente"
)
