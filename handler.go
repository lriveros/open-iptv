package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/lriveros/open-iptv/models"
)

func loginHandler(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("user_username")
	pass := r.FormValue("user_password")
	log.Print("Logeando al usuario " + name)
	var ctx Context
	if name != "" && pass != "" {
		usuario, err := models.Login(name, encriptarPassword(pass))
		ctx = getSessionContext(r)
		if err == nil {
			if usuario.Name == "" {
				ctx.Message = getErrorMsge("Combinacion de usuario/password incorrecta")
			}

			ctx.User = usuario
			setSession(ctx, w)
		} else {
			log.Panic(err)
		}
		//renderTemplate(w, "index", usuario, r)
	}
	renderTemplate(w, r, "index", ctx)
}

func editChannelHandler(w http.ResponseWriter, r *http.Request) {
	idChannel, err := strconv.Atoi(r.FormValue("id_channel"))

	if err != nil {
		log.Print(err)
		http.Redirect(w, r, "/", 302)
		return
	}
	ctx := getSessionContext(r)
	ctx.Channel, _ = models.GetChannel(idChannel)
	setSession(ctx, w)
	renderTemplate(w, r, "editChannel", ctx)
}

func searchChannelHanler(w http.ResponseWriter, r *http.Request) {
	word := r.FormValue("search_word")
	if word != "" {
		ctx := getSessionContext(r)
		ctx.Channels, _ = models.GetChannelList(word)
		setSession(ctx, w)
		renderTemplate(w, r, "index", ctx)
	} else {
		http.Redirect(w, r, "/", 302)
	}
}

// logout handler
func logoutHandler(w http.ResponseWriter, r *http.Request) {
	clearSession(w)
	//context.User = models.NewUser()
	//context.Channels, _ = models.GetChannelList("")
	http.Redirect(w, r, "/", 302)
}

func indexPageHandler(w http.ResponseWriter, r *http.Request) {
	ctx := getSessionContext(r)
	setSession(ctx, w)
	renderTemplate(w, r, "index", ctx)
}

func personalesHandler(w http.ResponseWriter, r *http.Request) {
	//var usuario models.User
	log.Print("Datos Personales")
	usuario := getSessionUser(r)

	if usuario.ID != 0 {
		ctx := getSessionContext(r)
		renderTemplate(w, r, "personales", ctx)
	} else {
		http.Redirect(w, r, "/", 302)
	}

}

func getListHandler(w http.ResponseWriter, r *http.Request) {
	//fmt.Fprintf(response, indexPage)
	go counts.Inc(nameCountGetList)
	http.ServeFile(w, r, mainListFile)
}

func showCounterHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("Contador de getList: ", counts.Value(nameCountGetList))
	http.Redirect(w, r, "/", 302)
}

func addLinkHandler(w http.ResponseWriter, r *http.Request) {
	link := r.FormValue("channel_link")
	idChannel, err := strconv.Atoi(r.FormValue("channel_id"))

	if err != nil {
		log.Print(err)
		http.Redirect(w, r, "/", 302)
		return
	}

	_, err = url.ParseRequestURI(link)
	if err != nil {
		log.Panic(err)
		http.Redirect(w, r, "/", 302)
	} else {
		u := getSessionUser(r)
		idUser := string(u.ID)
		_, _ = models.InsertLink(idChannel, link, idUser)
		//context.Channel.Links, _ = models.GetLinks(idChannel)
		//log.Print(context.Channel)
	}
	ctx := getSessionContext(r)
	ctx.Channel.Links, _ = models.GetLinks(ctx.Channel.ID)
	setSession(ctx, w)
	renderTemplate(w, r, "editChannel", ctx)
}

func actionLinkHandler(w http.ResponseWriter, r *http.Request) {
	idLink, err := strconv.Atoi(r.FormValue("id_link"))
	typeVote := r.FormValue("action")
	u := getSessionUser(r)

	if err != nil {
		log.Print(err)
		http.Redirect(w, r, "/", 302)
		return
	}

	if u.ID == 0 {
		http.Redirect(w, r, "/", 302)
		return
	}

	var code int
	if typeVote == "up" {
		code = voteUpLinkCode
	} else if "down" == typeVote {
		code = voteDownLinkCode
	} else if "inactive" == typeVote {
		code = voteInactiveCode
	}

	_, err = models.InsertLinkAction(idLink, u.ID, code)

	if err != nil {
		log.Print(err)
		http.Redirect(w, r, "/", 302)
		return
	}
	ctx := getSessionContext(r)
	ctx.Channel.Links, _ = models.GetLinks(ctx.Channel.ID)
	setSession(ctx, w)
	renderTemplate(w, r, "editChannel", ctx)
}

func actionChannelHandler(w http.ResponseWriter, r *http.Request) {
	idChannel, err := strconv.Atoi(r.FormValue("id_channel"))
	typeVote := r.FormValue("action")
	u := getSessionUser(r)

	if err != nil {
		log.Print(err)
		http.Redirect(w, r, "/", 302)
		return
	}

	if u.ID == 0 {
		http.Redirect(w, r, "/", 302)
		return
	}

	var code int
	if typeVote == "up" {
		code = voteUpLinkCode
	} else if "down" == typeVote {
		code = voteDownLinkCode
	} else if "edit" == typeVote {
		editChannelHandler(w, r)
		return
	}

	_, err = models.InsertChannelAction(idChannel, u.ID, code)
	if err != nil {
		log.Print(err)
		http.Redirect(w, r, "/", 302)
		return
	}
	ctx := getSessionContext(r)
	ctx.Channel, _ = models.GetChannel(ctx.Channel.ID)
	setSession(ctx, w)
	renderTemplate(w, r, "index", ctx)
}

func registerHandler(w http.ResponseWriter, r *http.Request) {
	var user models.User
	user.Username = r.FormValue("user")
	user.Name = r.FormValue("fName")
	user.LastName = r.FormValue("lName")
	user.Email = r.FormValue("emailId")
	user.Password = encriptarPassword(r.FormValue("password"))

	resp, err := models.InsertUser(user)

	if err != nil {
		log.Print(err)
	}

	ctx := getSessionContext(r)
	ctx.Message.ShowMessage = true
	if resp {
		ctx.Message.TypeMsge = statusOk
		ctx.Message.Title = "Usuario Ingresado Correctamente"
		ctx.Message.Detail = msgUsuarioRegOk
	} else {
		ctx.Message.TypeMsge = statusError
		ctx.Message.Title = "El Usuario no se ha podido Ingresar"
		ctx.Message.Detail = msgUsuarioRegNoOk
	}
	//log.Print(ctx)
	setSession(ctx, w)

	renderTemplate(w, r, "index", ctx)
}

func addChannelHandler(w http.ResponseWriter, r *http.Request) {
	var chnl models.Channel
	var cat int

	chnl.Name = r.FormValue("chnl_name")
	cat, err := strconv.Atoi(r.FormValue("chnl_category"))
	chnl.Description = r.FormValue("chnl_description")

	ctx := getSessionContext(r)

	log.Println("Insertando nuevo canal", chnl)

	if err != nil {
		log.Print(err)
		ctx.Message = getMsge("El Canal no ha podido ser agregado", "Error al agregar Canal")

		renderTemplate(w, r, "index", ctx)
		return
	}
	_, handler, err := r.FormFile("chnl_logo")
	fileName := getValidFileName(handler.Filename)
	chnl.Logo = fileName

	resp, err := models.InsertChannel(chnl, cat)

	if resp {
		log.Print("Subiendo logo al servidor")
		writeFileUploaded(r)
	} else {
		log.Println(err)
	}
	http.Redirect(w, r, "/", 302)
}

func writeFileUploaded(r *http.Request) error {
	r.ParseMultipartForm(32 << 20)
	file, handler, err := r.FormFile("chnl_logo")
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer file.Close()
	filename := getValidFileName("./static/imgs/channels_logo/" + handler.Filename)
	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer f.Close()
	io.Copy(f, file)
	return nil
}

func getValidFileName(fileName string) string {
	fileNameAux := fileName
	var cont int
	for {
		cont++
		//Si el archivo ya existe, le cambiamos el nombre
		if _, err := os.Stat(fileNameAux); err == nil {
			log.Println("Filename exists :", fileNameAux)
			arr := strings.Split(fileName, ".")
			arr[len(arr)-1] = strconv.Itoa(cont) + "." + arr[len(arr)-1]
			fileNameAux = strings.Join(arr, ".")
			log.Println("New Filename :", fileNameAux)
		} else {
			break
		}
	}
	return fileNameAux
}

func getErrorMsge(msge string) Message {
	return getMsge(msge, "Error al realizar la accion")
}

func getMsge(msge string, title string) Message {
	var m Message
	m.Title = title
	m.Detail = msge
	m.ShowMessage = true
	m.TypeMsge = statusError
	return m
}
